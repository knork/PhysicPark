# Installation

```
sudo apt-get install libglfw3-dev libglm-dev libglew-dev libfreetype6-dev libassimp-dev git cmake
git clone git@gitlab.com:knork/PhysicPark.git
cd physicPark
cmake .
make
```

## LightCollection and Lights

To avoid setting uniform values for the different shader for
each light all lights are managed in the LightCollection class
and send to the GPU as a UniformBufferObject so that all Shader can
access it.
set in constructor:
UBO Binding 0 Matrices (ctm etc.)
UBO Location 1 LIGHTS

```c++
struct Light{
    vec3 ambient; // 16
    vec3 position; // 16
    int type;  //
    float spec; // 16
    // Total 48 Bytes
};
// these sit at index 1
layout (std140) uniform Lights{
    int amount; // 4 Bytes ... somehow it's actually 16 ?!? alignemd?!?
    Light lights[MAX]; // 30 * 48 = 1440
    // Total 1444 Bytes
};
```


## Shader

Use makeShader()-method of the Scene class. This allows the scene
to keep track of what shaders to prevent duplicates.

When adding objects to the scene you have to pass that shader
back to the scene so the scene knows what objects wants
what shader. This allows the scene to group up all objects that
belong to a shader and thus reduce state changes.

Sample of create a Minimalisitc Scene
```C++
Scene scene;
Shader& shader = scene.makeShader("v.glsl","f.glsl");
DrawPtr sphere(new Sphere (glm::vec3(0.0f,0.0f,-0.0f),glm::vec3(0,0,0),0));
scene.addP(shader,std::move(sphere));
```


## Camera

If anything other than the default Camera is desired it also needs
to be set in the Scene-Description.


