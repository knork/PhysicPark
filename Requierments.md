## Requierments


## Multithreading

Every thread has it's own result vector that
sums the dX for each particle. After threading all
changes are summed together.

## Instancing

We want to instance as much as possible without doing anything manual in
the definition file.
But at the same time i need the ability to use diffrent shaders (as the
water will mostlikly need a different shader from the rest)

Instancing the whole primitive would work for everything but meshes.
Which again will be needed for water...

Thus for now as i do not load any general meshes i will
implement primitves to use instancing automatically.

And worry about instacing meshes if i actually need to do that.

## Shader

We should be able to choose different Shader for different Objects.

Options:

To reduce the amount of expensive openGL calls (like glSetProgram()) we should group objects by shader
that they use. In order to so each sceneObj is associated with an index that corresponds to the shaderIndex
from the array where all Shaders are saved (scene.allShaders).


## Constraints

Constraints need to independent of when particles are added to the particle system.
Thus Collision Constraints for example need to apply even to particle that are added later
into the scene (maby through new fluid particles). Constraint are created by Objects.

Options:

- Have a group of Objects that are responsible for inter-object constraints and give them the 
option to update the constraints after new particles are added. Would further requiere to group
 spawning of new particles.
- Have an extra group for constraints that globally apply (to **all** particles)
    - would also apply to other particles of the same object
- regenerate all constraints before update()

All options have some constraints that need to be updated every time step. For example
collision between "rigid bodies" and other particles. (Since the rigid body might move).
However if the rigid Body is made up of particles these constraints stay the same since they
refrence to the particles which in turn may change positions.


### Evaluating the global constraint model

Take constraint as an ABC which represents a set of constraint, for example constraint to apply
collision between Object A and Object B (remember each object may contain any amount of particles). 
Each "constraint" knows which particles it enacts upon and is given access to the particle array (or lookuptable)to
apply it's constraint.

We require each such constraint to either act on **all** particles or update it's effected particle each
 timestep.
 
 Constraint may look like this?
 
 Const:All
 Const:const
 






