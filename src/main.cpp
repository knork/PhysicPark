#include <glad/glad.h>
#include "drawing/Utils.h"
#include "drawing/DrawableGeometry.h"
#include "drawing/Scene.h"
#include "scenes/SceneDefinitions.h"
#include <unistd.h>


int main() {

    // There may only be a single window currently
    Window win;
    // loadScene()
    Scene scene(win);
    SceneDefinitions::second(scene);
//    Scene scene = std::move(SceneDefinitions::first());

    glEnable(GL_DEPTH_TEST );
//    glEnable (GL_BLEND);
//    glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
//    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//    glBlendFunc(GL_ZERO, GL_SRC_COLOR);

//    glFrontFace(GL_CCW);
//    glEnable(GL_CULL_FACE);
//    glCullFace(GL_FRONT);
    while(!win.shoudlClose()){

        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        scene.update(win.getDt());
        scene.render();
//        scene.update();
        win.update();
//        std::cout <<(1.0f/win.getDtUnpausable())<<std::endl;

    }
    return 0;
}
