#version 330 core

in VS_OUT{
    vec3 FragPos; // world coords
    vec3 norm;
    vec3 color;
//    vec2 TexCoords;
//	mat3 TBN;
} fs_in;

layout (std140)uniform Matrices{
                    // Base align     // aligned offset
    mat4 projection; // 4x16          // 0
    mat4 view; // 4x 16bytes          // 64
    mat4 projView; // 4x 16 bytes     // 128
    vec3 viewPos; // 16 Bytes
    // Total 208 Bytes
};

#define MAX 30


struct Light{
    vec3 ambient; // 16
    vec3 position; // 16
    int type;  //
    float spec; // 16
    // Total 48 Bytes
};
// these sit at index 1
layout (std140) uniform Lights{
    int amount; // 4 Bytes ... somehow it's actually 16 ?!? why ? dunno
    Light lights[MAX]; // 30 * 48 = 1440
    // Total 1444 Bytes
};



out vec4 color;



vec3 pointLight(int index,vec3 norm){
    // light dir is in eye coords
    vec3 lightDir = -(fs_in.FragPos - lights[index].position);
    float d = length(lightDir);
    lightDir = normalize ( lightDir);

    float diff = max(dot(lightDir,norm ), 0.0);
    vec3 diffuse = diff *fs_in.color;

//    // direction to the viewer
    vec3 viewDir = normalize( (viewPos - fs_in.FragPos));
    vec3 reflectDir = reflect(-lightDir,norm);
    float specAm = pow(max(dot(viewDir, reflectDir), 0.0),256);
    vec3 specular =  4*specAm * (vec3(1.0,0.5,0.2));
    return diffuse+specular;
}
void main() {

    vec3 tmp=vec3(0,0,0);
    vec3 norm = normalize(fs_in.norm);
    for(int i =0;i<MAX;i++){
        if(i<amount)
            tmp += pointLight(i,norm) ;
    }

//    color = vec4(0.0,0.0,0.5,1.0);
//    color = vec4(vec3(0.1,0.2,0.9)*tmp,0.6);
    color = vec4(tmp,0.8);
//    color=vec4(fs_in.color,1.0);
//    color = vec4(lights[0].position,1);
//        color = vec4(amount,0,0,1);
}
