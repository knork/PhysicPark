#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;

//layout (location = 7) in vec3 tangent;

uniform mat4 ctm;
uniform vec4 color;
uniform mat4 model;


layout (std140)uniform Matrices{
                    // Base align     // aligned offset
    mat4 projection; // 4x16          // 0
    mat4 view; // 4x 16bytes          // 64
    mat4 projView; // 4x 16 bytes     // 128
    vec3 viewPos; // 16 Bytes
    // Total 208 Bytes
};


out VS_OUT{
    vec3 FragPos;   // eye coords
    vec3 norm;      // world coords
    vec3 color;
//    vec2 TexCoords;
//    vec3 TBN;
} vs_out;





void main() {
    gl_Position = projView * model * vec4(position, 1.0f);

    vs_out.FragPos = ( model * vec4(position,1.0f)).xyz;
    vs_out.norm = (inverse(transpose(model))*vec4(normal,0.0f)).xyz;
    vs_out.color=vec3(0.2,0.1,0.6);

//    vs_out.norm=normal;

//    vec3 T = normalize(vec3(model * vec4(tangent, 0.0)));
//    vec3 N = normalize(vec3(model * vec4(normal, 0.0)));
    // re-orthogonalize T with respect to N
//    T = normalize(T - dot(T, N) * N);
//    // then retrieve perpendicular vector B with the cross product of T and N
//    vec3 B = cross(T, N);
//
//    vs_out.TBN = mat3(T, B, N);

}
