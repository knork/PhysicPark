#version 330 core

in VS_OUT{
    vec3 FragPos; // world coords
    vec3 norm;
//    vec2 TexCoords;
//	mat3 TBN;
} fs_in;


// binding point 0
layout (std140) uniform Matrices{
                    // Base align     // aligned offset
    mat4 projection; // 4x16          // 0
    mat4 view; // 4x 16bytes          // 64
    mat4 projView; // 4x 16 bytes     // 128
    vec3 viewPos; // 16 Bytes
    // Total 208 Bytes
};

struct Light{
    vec3 ambient; // 16
    vec3 position; // 16
    int type;  //
    float spec; // 16
    // Total 48 Bytes
};

#define MAX 30
// Binding point 1
layout (std140) uniform Lights{
    int amount; // 4 Bytes ... somehow it's actually 16 ?!? why ? dunno
    Light lights[MAX]; // 30 * 48 = 1440
    // Total 1444 Bytes
};

in vec4 col;
out vec4 color;

vec3 pointLight(int index,vec3 norm){

    // light dir is in eye coords
//    vec3 lightDir = (view*vec4(lights[index].position,1.0)).xyz-fs_in.FragPos;
    vec3 lightDir = -(fs_in.FragPos - lights[index].position);
    float d = length(lightDir);
    lightDir = normalize ( lightDir);

//    lightDir = normalize ( fs_in.TBN *lightDir);
//    vec3 amb = lights[index].ambient * vec3(texture(texture_diffuse1,fs_in.TexCoords));

    float diff = max(dot(lightDir,norm ), 0.0);
    vec3 diffuse = diff *vec3(1,1,1);


//    // direction to the viewer
//    vec3 viewDir = normalize( fs_in.TBN *(viewPos - fs_in.FragPos));
//    vec3 reflectDir = reflect(-lightDir,norm);
//    float specAm = pow(max(dot(viewDir, reflectDir), 0.0),120);
//    vec3 specular =   specAm * (vec3(texture(texture_specular1,fs_in.TexCoords)));
    return diffuse;
}
void main() {


    vec3 tmp=vec3(0,0,0);
    for(int i =0;i<MAX;i++){
        if(i<amount)
            tmp += pointLight(i,fs_in.norm) ;
    }

//    color = vec4(0.5,0.5,0.5,1.0);
    color = vec4(tmp,1);
//    color = vec4(lights[0].position,1);
//        color = vec4(amount,0,0,1);
}
