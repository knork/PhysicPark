#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in mat4 instanceMatrix;
uniform mat4 ctm;
//layout (location = 7) in vec3 tangent;


layout (std140) uniform Matrices{
                    // Base align     // aligned offset
    mat4 projection; // 4x16          // 0
    mat4 view; // 4x 16bytes          // 64
    mat4 projView; // 4x 16 bytes     // 128
    vec3 viewPos; // 16 Bytes
    // Total 208 Bytes
};

out VS_OUT{
    vec3 FragPos;   // eye coords
    vec3 norm;      // world coords
//    vec2 TexCoords;
//    mat3 TBN;
} vs_out;

out vec4 col;



void main() {
//    gl_Position = ctm*instanceMatrix*  vec4(position, 1.0f);
    gl_Position = projView*instanceMatrix*  vec4(position, 1.0f);

    vs_out.FragPos = (instanceMatrix*vec4(position,1.0f)).xyz;
//    vs_out.norm = (inverse(transpose(model))*vec4(normal,0.0f)).xyz;
    vs_out.norm=normal;
}
