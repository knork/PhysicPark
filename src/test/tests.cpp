//
// Created by knork on 6/19/17.
//
#include <gtest/gtest.h>

int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}