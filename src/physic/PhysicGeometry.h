//
// Created by knork on 6/24/17.
//

#ifndef FLUIDSIM_PHYSICGEOMETRY_H
#define FLUIDSIM_PHYSICGEOMETRY_H


#include <glm/vec3.hpp>
#include <vector>
#include <bits/unique_ptr.h>
#include "../drawing/Utils.h"
#include "Constraint.h"
#include "SpacialHash.h"


class Scene;
class PhysicsObject;
class Plane;

///Default Values

const float DEFAUTLT_PARTICLE_SIZE = 0.2f;

/**
 * Represents an abstract entity in the scene. Used to have a commin interface
 * to both drawable objects and Physical Objects (ParticleObj)
 */
class SceneObj{
protected:
    glm::vec3 position;
public:
    SceneObj():position(0){}
    SceneObj(glm::vec3 pos):position(pos){}

    virtual ~SceneObj(){}
    virtual const glm::vec3& getPosition()const{return position;}
    virtual  glm::vec3& getPosition(){return position;}
    virtual glm::vec3 setPosition(glm::vec3 pos){
        bool nan = std::isnan((pos.x+pos.y+pos.z));
        position=pos;
    }
};


class Particle:public SceneObj{
public:
    Particle(glm::vec3 pos=glm::vec3(0,0,0),glm::vec3 vel=glm::vec3(0,0,0),float weight=1.0):SceneObj(pos),velocity(vel),w(weight),iw(1.0f/weight){}
    glm::vec3 velocity;
//    glm::f64vec3 velocity;
    /// uncorrected Position
    glm::vec3 prevPos;
    // inverted mass
    float w=1.0,iw=1.0,radius=DEFAUTLT_PARTICLE_SIZE;

    bool isValid();
};


/**
 * Owner of all PhysicObjects and manages their updates and constraints
 */
class ParticleSystem{

    std::vector<std::unique_ptr<PhysicsObject>> allObj;
    ObjectMap globalHashMap;
    Scene& scene;
    // External forces
    float gravity=140.3;

public:

    ParticleSystem(Scene& sc);
    void add(std::unique_ptr<PhysicsObject> obj);
    void update(float dt);
    void drawOrInstance() const;
};

/**
 * Represent an object that is made up of particles and can effect other
 * PhysicObjects thorugh constraints. (unlike drawable objects which are only drawn but don't interact)
 */
class PhysicsObject{
protected:

    std::vector<Particle> allParticles;
    /// since we want to use abstrtaction we have to use pointers
    /// since refrences can not be stored in vectors
    std::vector<std::unique_ptr<Constraint>> constraints;

public:
    PhysicsObject(Scene& scene);
    virtual void afterVeloUpdate(float dt)=0;
    virtual void preConstraints(float dt)=0;
    virtual void postConstraints(float dt)=0;
    /**
     * The Object either draws itself or adds instances to the static InstancedSphere
     * which will draw all instances at once
     */
    virtual void drawOrAdd() const;

    virtual const std::vector<Particle>& getAllParticles()const{return allParticles;}

    virtual std::vector<Particle>& getAllParticles(){return allParticles;}

    virtual std::vector<std::unique_ptr<Constraint>>& getConstraints(){return constraints;};
};



/**
 * Simplest actual PhysicsObject. Only a sphere that keeps it's distance from other particles
 */
class Simple: public PhysicsObject{
    float size;
public:
    Simple(Scene &scene, glm::vec3 pos, float size, float weight);
    void preConstraints(float dt) override;
    void postConstraints(float dt) override;
    void afterVeloUpdate(float dt) override;
    void drawOrAdd() const override;
};

class Wall: public  PhysicsObject{
    std::unique_ptr<Plane> plane;
    glm::mat4 ori;
    Shader& shader;
public:
    Wall(Scene &scene, int axis, glm::vec3 pos);
    void preConstraints(float dt) override;
    void postConstraints(float dt) override;
    void afterVeloUpdate(float dt) override;
    void drawOrAdd() const override;
};

class Box:public PhysicsObject{
public:
    Box(Scene& scene, glm::vec3 min, glm::vec3 max);
    void drawOrAdd() const override;
    void afterVeloUpdate(float dt) override;
    void preConstraints(float dt) override;
    void postConstraints(float dt) override;
};

#endif //FLUIDSIM_PHYSICGEOMETRY_H
