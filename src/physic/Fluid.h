//
// Created by knork on 7/8/17.
//

#ifndef FLUIDSIM_FLUID_H
#define FLUIDSIM_FLUID_H


#include "PhysicGeometry.h"

#include "../drawing/myMarchingCube.h"

class Fluid : public PhysicsObject{
    void doubleRelax(float dt);
    void applyVisco(float dt);
    void fill(int amount,float step);
protected:
    double toggleTime;
    bool mc=true,part=false;


    /// position and velocity of spawning particles
    Shader& shader;
    glm::vec3 spawnPos,spawnVelo;
    ParticleMap localMap;
    mutable myMarchingCube cube;
    Scene& scene;
//    mutable MarchingCubes marching;
//    float rho_0=3.5, k=10.1, k_near=2.7, alpha=0.3 ,beta=1,h=3.9,h2=h*h;
    float rho_0=3.5, k=70.1, k_near=140.7, alpha=0 ,beta=1,h=3.9,h2=h*h;

public:
    Fluid(Scene &scene, glm::vec3 pos, glm::vec3 vel, size_t amount);
    void afterVeloUpdate(float dt) override;
    void preConstraints(float dt) override;
    void postConstraints(float dt) override;


    float getRho_0() const;

    void setRho_0(float rho_0);

    float getK() const;

    void setK(float k);

    float getK_near() const;

    void setK_near(float k_near);

    float getAlpha() const;

    void setAlpha(float alpha);

    float getBeta() const;

    void setBeta(float beta);

    void toggleParticleDisplay();
    void toggleMarchingCube();

    float getH() const;

    void setH(float h);

    void reset();

    void drawOrAdd() const override;

    float getDensity(glm::vec3 pos)const;
};
#endif //FLUIDSIM_FLUID_H
