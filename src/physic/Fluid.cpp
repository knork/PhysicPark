//
// Created by knork on 7/8/17.
//

#include <omp.h>
#include "Fluid.h"


#include <cmath>
#include <glm/gtx/norm.hpp>
#include "../drawing/Scene.h"
#include "../drawing/myMarchingCube.h"

Fluid::Fluid(Scene &scene, glm::vec3 pos, glm::vec3 vel, size_t amount) :
        PhysicsObject(scene), localMap(allParticles, 16 * DEFAUTLT_PARTICLE_SIZE),
        spawnPos(pos), spawnVelo(vel),
    shader(scene.makeShader("regualV.glsl","regularF.glsl")),cube(*this),scene(scene){
    float step = 2.5f * h * DEFAUTLT_PARTICLE_SIZE;
    fill(amount, step);


//    marching= MarchingCubes(*this,1,1,1);
}

void Fluid::reset() {
    // this allows us also to add more particles which will then be displayed and processed in
    // the next frame
    int amount = allParticles.size();
    allParticles.clear();
    float step = 2.5f * h * DEFAUTLT_PARTICLE_SIZE;
    fill(amount, step);
}

void Fluid::fill(int amount, float step) {
    int total = 0;
    for (int i = 0; total < amount; i++) {
        for (int j = 0; j < 8 && total < amount; j++) {
            for (int k = 0; k < 8 && total < amount; k++) {
                total++;
                allParticles.push_back(Particle(spawnPos + glm::vec3(k * step, i * step, j * step), spawnVelo));
            }
        }
    }

//    for(int i =0;i<amount;i++){
//        for(int j =0;j<amount;j++){
//            if(i!=j && glm::length2(allParticles[i].getPosition()-allParticles[j].getPosition())==0.0f){
//                std::cout << "Particles at the same position.."<<std::endl;
//            }
//        }
//    }
}

void Fluid::preConstraints(float dt) {
    doubleRelax(dt);
}

void Fluid::postConstraints(float dt) {

}

void Fluid::applyVisco(float dt) {

    localMap.updateMap();

#pragma omp parallel
    {
        std::vector<glm::vec3> delta(allParticles.size(), glm::vec3(0.0f));

#pragma omp for schedule(guided)
            for (int i = 0; i < allParticles.size(); i++) {
                std::vector<float> qs;
                std::vector<glm::vec3> normals;
                auto neighbors = localMap.getAround(allParticles[i], h, qs, normals);
                for (int j = 0; j < neighbors.size(); j++) {
                    unsigned long index = neighbors[j] - &allParticles[0];
                    if (i < index)
                        continue;

                    // hash map already eliminates all candidates that  are q>=1

                    float u = glm::dot((allParticles[i].velocity - neighbors[j]->velocity), normals[j]);
                    u = std::min(1.0e2f,u); // makes sure we won't get to inf when squared
                    if (u > 0) {
                        glm::vec3 dv = (dt * (1 - qs[j])) * (alpha * u + beta * u * u) * normals[j];
                        dv/=2.0f;
                        assert(!std::isnan(dv.x + dv.y + dv.z) && " Setting invalid value in viscosity function ");
                        allParticles[i].velocity -= dv *0.5f;
                        neighbors[j]->velocity += dv * 0.5f;
                    }

                }

            }
    }


}

void Fluid::doubleRelax(float dt) {
    std::vector<float> delta;
//    #pragma omp parallel num_threads(6)
#pragma omp parallel
    {
        std::vector<glm::vec3> delta(allParticles.size(), glm::vec3(0.0f));


#pragma omp for schedule(guided)
        for (int i = 0; i < allParticles.size(); i++) {
            double rho = 0.0, rho_near = 0.0;
            //stores q
            std::vector<float> qs;
            //stores normals
            std::vector<glm::vec3> normals;
            auto neighbors = localMap.getAround(allParticles[i], h, qs, normals);

            // clac density
            for (int j = 0; j < neighbors.size(); j++) {
                float q2 = (1 - qs[j]) * (1 - qs[j]);
                rho = rho + q2;// density
                rho_near = rho_near + q2 * (1 - qs[j]); // near density
            }
            float Pr = (rho - rho_0) * k;
            float P_near = rho_near * k_near;


            glm::vec3 dx(0, 0, 0);
            // apply displacement
            for (int j = 0; j < neighbors.size(); j++) {
                auto &p = neighbors[j];

                float Pr_cur = Pr * (1 - qs[j]);
                float Pr_near_cur = P_near * (1 - qs[j]) * (1 - qs[j]);
                glm::vec3 D = dt * dt * (Pr_cur + Pr_near_cur) * normals[j];
                D *= 0.5f;
                glm::vec3 next = p->getPosition() + D;
                p->setPosition(next); // probably major performance problem here
                dx = dx - D;
            }
            delta[i] += dx;
//        allParticles[i].setPosition(allParticles[i].getPosition() + dx);
        }
#pragma omp critical
        {
            for (int i = 0; i < delta.size(); i++) {

                allParticles[i].setPosition(allParticles[i].getPosition() + delta[i]);
            }
        }
    }
//    std::cout << getDensity({1,1,1}) <<std::endl;

}

void Fluid::afterVeloUpdate(float dt) {
    applyVisco(dt);

}

float Fluid::getRho_0() const {
    return rho_0;
}

void Fluid::setRho_0(float rho_0) {
    Fluid::rho_0 = rho_0;
}

float Fluid::getK() const {
    return k;
}

void Fluid::setK(float k) {
    Fluid::k = k;
}

float Fluid::getK_near() const {
    return k_near;
}

void Fluid::setK_near(float k_near) {
    Fluid::k_near = k_near;
}

float Fluid::getAlpha() const {
    return alpha;
}

void Fluid::setAlpha(float alpha) {
    Fluid::alpha = alpha;
}

float Fluid::getBeta() const {
    return beta;
}

void Fluid::setBeta(float beta) {
    Fluid::beta = beta;
}

float Fluid::getH() const {
    return h;
}

void Fluid::setH(float h) {
    Fluid::h = h;
    h2=h*h;
}

void Fluid::drawOrAdd() const {
    if(part)
        PhysicsObject::drawOrAdd();

    if(!mc)
        return;

    glm::vec3 max = localMap.max;
    max.y = std::min(localMap.max.y,40.0f);
//    std::vector<Vertex> res = cube.updateIsoValues(1.7,localMap.min,localMap.max+glm::vec3(0,2,0));
    std::vector<Vertex> res = cube.updateIsoValues(2.0,glm::vec3(0,0,0),glm::vec3(17,60,75),glm::vec3(15,40,75));
    std::vector<GLuint> some;

//    if(res.size()!=0)
//      std::cout << "Fluid: vertices: " << res.size()<<std::endl;
    Mesh mesh(res,some);
    mesh.color=glm::vec4((0.1f,0.2f,0.0f));
    mesh.render(shader);

}

float Fluid::getDensity(glm::vec3 pos) const {
    auto neighbors = localMap.getAround(pos,h);
    float sum=0;
    float cur=0;

    for(auto&p :neighbors){
        float d = glm::length2(p->getPosition()-pos);
        cur= (1 - d/(h2));
        sum+=cur*cur*cur;
    }
    sum = std::sqrt(sum);
    assert(!std::isnan(sum));
    return sum;
}



void Fluid::toggleParticleDisplay() {
    if(glfwGetTime()-toggleTime<0.2)
        return;
    toggleTime=glfwGetTime();
    part=!part;
}

void Fluid::toggleMarchingCube() {
    if(glfwGetTime()-toggleTime<0.2)
        return;
    toggleTime=glfwGetTime();
    mc=!mc;
}
