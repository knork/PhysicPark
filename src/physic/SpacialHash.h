//
// Created by knork on 7/5/17.
//

#ifndef FLUIDSIM_SPACIALHASH_H
#define FLUIDSIM_SPACIALHASH_H


#include <bits/unique_ptr.h>
#include <vector>
#include <glm/vec3.hpp>
#include <unordered_map>


class PhysicsObject;

class Particle;

class SpacialHash {
protected:
    float gridSpacing;
    float iGridSpacing;

    std::unordered_multimap<int64_t, Particle *> hashMap;

    inline int64_t getHash(glm::vec3 pos) const {
        u_int16_t x = pos.x * iGridSpacing;
        u_int16_t y = pos.y * iGridSpacing;
        u_int16_t z = pos.z * iGridSpacing;
        // a / b is int(a) / int(b) for >=, ie,
        //bins are [**,**)
        if (pos.x < 0.0) { x--; }
        if (pos.y < 0.0) { y--; }
        if (pos.z < 0.0) { z--; }
        // there are 16 bits unused that could give us a larger grid
        // that would however cost us a little performance
        // or __int128 / 128bit struct
        int64_t out = 0;
        out += x;
        out = out << 16;
        out += y;
        out = out << 16;
        out += z;
        return out;
    }

public:
    /**
     * @param gridSpacing since map get's updated every frame gridspace could also be changed
     * later on, as particle are placed into the grid every frame
     */
    SpacialHash(float gridSpacing);
    const std::unordered_multimap<int64_t, Particle *> &getAllParticles() const;
    virtual void updateMap()=0;

    std::vector<Particle *> getAround(glm::vec3 point, float radius) const;

    /**
     *
     * @param point - that we wish to obtain the neighbors for
     * @param h - like the radius. If q<1 particle is considered a neighbor
     * @param qs - q = ( | point - neighbors | / h )
     * @param normal - normilized normals will be calculated and stored here
     * @return Pointer to all particles within defined "radius" h
     */
    std::vector<Particle *>
    getAround(Particle &point, float h, std::vector<float> &qs, std::vector<glm::vec3> &normal);

};

class ObjectMap : public SpacialHash {
    std::vector<std::unique_ptr<PhysicsObject>> &allObj;

public:
    ObjectMap(std::vector<std::unique_ptr<PhysicsObject>> &allObjects, float gridSpacing);

    void updateMap() override;
};

class ParticleMap : public SpacialHash {

    std::vector<Particle> &allParticles;
public:
    ParticleMap(std::vector<Particle> &particles, float gridSpacing);
    glm::vec3 max,min;
    void updateMap() override;
};


#endif //FLUIDSIM_SPACIALHASH_H
