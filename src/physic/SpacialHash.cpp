//
// Created by knork on 7/5/17.
//

#include <iostream>
#include "SpacialHash.h"
#include "PhysicGeometry.h"
#include <glm/gtx/norm.hpp>

SpacialHash::SpacialHash(float gridSpacing)
        : gridSpacing(gridSpacing), iGridSpacing(1.0f / gridSpacing) {

}


const std::unordered_multimap<int64_t, Particle *> &SpacialHash::getAllParticles() const {
    return hashMap;
}


std::vector<Particle *> SpacialHash::getAround(glm::vec3 point, float radius) const {

    std::vector<Particle *> ret;
    int64_t hash;
    float i,j,k;
    float rad2 = radius*radius;
    for ( i = point.x - radius; i < point.x + radius; i += gridSpacing) {
        for ( j = point.y - radius; j < point.y + radius; j += gridSpacing) {
            for ( k = point.z - radius; k < point.z + radius; k += gridSpacing) {
                hash = getHash(glm::vec3(i, j, k));
                const auto &range = hashMap.equal_range(hash);
                for (auto it = range.first; it != range.second; ++it) {
                    if (glm::length2(it->second->getPosition() - point) < rad2) {
                        ret.push_back(it->second);
                    }
                }
            }
        }
    }
    return ret;
}

std::vector<Particle *>
SpacialHash::getAround(Particle &par, float h, std::vector<float> &qs, std::vector<glm::vec3> &normal) {

    float radius = h;

    std::vector<Particle *> ret;
    int64_t hash;
    glm::vec3 point = par.getPosition();

    Particle* first = &par;

    int count = 0;
    /// for really big numbers adding a small grid space might be light adding 0
    /// also hashing for bigger numbers (anything above 2^16 /2 *gridSpace) will be problematic
    if (glm::length2(point) > (32000.0 * 32000.0)) {
        return ret;
    }



    float i,j,k;
    for ( i = point.x - radius; i < point.x + radius; i += gridSpacing) {
        for ( j = point.y - radius; j < point.y + radius; j += gridSpacing) {
            for ( k = point.z - radius; k < point.z + radius; k += gridSpacing) {

                hash = getHash(glm::vec3(i, j, k));
                const auto &range = hashMap.equal_range(hash);
                for (auto it = range.first; it != range.second; ++it) {
                    Particle* second = it->second;
                    if (&par == second)
                        continue;
                    glm::vec3 pi_pj = second->getPosition() - point;
                    float length = glm::length(pi_pj);
                    float q = length / h;
                    if (q < 1.0f) {
                        qs.push_back(q);
                        ret.push_back(second);
                        if (length != 0.0)
                            normal.push_back(pi_pj * (1.0f / length));
                        else
                            normal.push_back(pi_pj);
                    }
                }
            }
        }
    }
    return ret;

}

void ObjectMap::updateMap() {
    hashMap.clear();



    for (auto &obj : this->allObj) {
        for (auto &p:obj->getAllParticles()) {
            const int64_t hash = getHash(p.getPosition());
            std::pair<int64_t, Particle *> in(hash, &p);
            hashMap.insert(std::move(in)); // no need to reallocate hopfully
        }
    }
}

ObjectMap::ObjectMap(std::vector<std::unique_ptr<PhysicsObject>> &allObjects, float gridSpacing) :
        SpacialHash(gridSpacing), allObj(allObjects) {
}

ParticleMap::ParticleMap(std::vector<Particle> &particles, float gridSpacing) :
        SpacialHash(gridSpacing), allParticles(particles) {

}

void ParticleMap::updateMap() {
    hashMap.clear();

    min = glm::vec3(std::numeric_limits<float>::max(),std::numeric_limits<float>::max(),std::numeric_limits<float>::max());
    max= glm::vec3(std::numeric_limits<float>::min(),std::numeric_limits<float>::min(),std::numeric_limits<float>::min());
    for (auto &p:allParticles) {
        int64_t hash = getHash(p.getPosition());
        std::pair<int64_t, Particle *> in(hash, &p);
        hashMap.insert(std::move(in));
        max.x = max.x>p.getPosition().x?max.x:p.getPosition().x;
        max.y = max.y>p.getPosition().y?max.y:p.getPosition().y;
        max.z = max.z>p.getPosition().z?max.z:p.getPosition().z;
        min.x = min.x<p.getPosition().x?min.x:p.getPosition().x;
        min.y = min.y<p.getPosition().y?min.y:p.getPosition().y;
        min.z = min.z<p.getPosition().z?min.z:p.getPosition().z;
    }
}
