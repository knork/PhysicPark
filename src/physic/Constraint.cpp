//
// Created by knork on 7/3/17.
//

#include "Constraint.h"
#include "PhysicGeometry.h"

void AAInfPlaneConst::apply(const SpacialHash &hashMap) {


    auto &input = hashMap.getAllParticles();
    int absAxis = std::abs(axis) - 1; // get index corresponding to axis
    int count = 0;

    for (auto &k:input) {
        auto &p = k.second;

        glm::vec3 ab = glm::vec3(0);
        ab[absAxis] = (p->getPosition()[absAxis] - pos[absAxis]);
        float distanceToPlane = ab[absAxis];

        float y = p->getPosition()[1];

        /// when we get "too close"
        if ((axis < 0 && distanceToPlane + p->radius > 0)
            || (axis > 0 && distanceToPlane - p->radius < 0)) {

            glm::vec3 normal(0);
            normal[absAxis] = axis < 0 ? -1 : 1;
            // only works if the circle is not yet on "the other side..."
            float s = (p->radius - std::fabs(distanceToPlane));
            if (s < 0) {
                s = std::fabs(distanceToPlane);
            }
            glm::vec3 dX = normal * s;
            glm::vec3 cur = p->getPosition();
            glm::vec3 next = cur + dX;
            p->setPosition(next);
//            p->prevPos=cur;
        }
    }
}

void DistanceConstraint::apply(const SpacialHash &hashMap) {

    auto neighbors = hashMap.getAround(first.getPosition(), 0.99f * distance);

    float actualDistance;
    float iw = first.iw;
    for (auto &par:neighbors) {
        if (par == &first) { // don't process your're self
            continue;
        }
        float jw = par->iw;
        glm::vec3 ab = first.getPosition() - par->getPosition();
        float length = glm::length(ab);
        actualDistance = first.radius + par->radius;
        if (length > actualDistance || length < 0.0001f)
            continue;
        glm::vec3 normal = ab / length;
        float s = 0.0f;
        if (iw + jw > 0) {
            s = (length - actualDistance) / (iw + jw);
        }
        glm::vec3 diX, djX;
        diX = iw * -s * normal;
        djX = jw * s * normal;
        first.setPosition(first.getPosition() + diX);
        par->setPosition(par->getPosition() + djX);

    }

}


void BoxCollision::apply(const SpacialHash &hashMap) {


    auto &input = hashMap.getAllParticles();

    glm::vec3 p;
    glm::vec3 pM, pP;// p minus radius, p plus Radius

    Particle *particle;
    float radius,diff;

    for (auto &k:input) {
//        float r= static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
        particle = k.second;
        p = particle->getPosition();
        radius = particle->radius;
        pM = p - radius;
        pP = p + radius;

//        glm::vec3 prev = particle->prevPos;

        if (pM.x < pMin.x) {
            diff = pMin.x-pM.x;
            p.x += diff;
        } else if (pP.x > pMax.x) {
            diff = pP.x-pMax.x;
            p.x -= diff;
        }

        if (pM.y < pMin.y) {
            p.y = pMin.y + radius;
//            if(r<0.001){
//                p.y+=0.18f;
//            }
        } else if (pP.y > pMax.y) {
            p.y = pMax.y - radius;
        }

        if (pM.z < pMin.z) {
            p.z = pMin.z + radius;
        } else if (pP.z > pMax.z) {
            p.z = pMax.z - radius;
        }

        if (p != particle->getPosition())
            particle->setPosition(p);

    }


}
