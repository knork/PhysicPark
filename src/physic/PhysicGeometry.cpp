

//
// Created by knork on 6/24/17.
//

#include "PhysicGeometry.h"
#include "../drawing/DrawableGeometry.h"
#include "../drawing/Scene.h"
#include "../drawing/InstancedSphere.h"
#include <cmath>
#include <glm/gtx/norm.hpp>

void PhysicsObject::drawOrAdd() const {

//    Sphere sphere(glm::vec3(0, 0, 0), 1);
    glm::mat4 model;
    // Should be changed to instanced drawing at some point
    for (const Particle &p: allParticles) {
        glm::mat4 curModel = glm::translate(model, p.getPosition());
        curModel = glm::scale(curModel, glm::vec3(p.radius));
//        sphere.render(in, curModel);
        InstancedSphere::addInstance(curModel);
    }
}

PhysicsObject::PhysicsObject(Scene &scene) {
    std::unique_ptr<PhysicsObject> cur(this);
    scene.add(std::move(cur));
}


void ParticleSystem::update(float dt) {

    /// if dt==0 we do "nothing" => can skip
    if (dt < 0.00001f)
        return;


    /// 1. apply external forces for each particle
    /// by numerical integration
    u_int i, j, k, amount;

#pragma omp parrallel
    {
        for (auto &obj:allObj) {
            amount = obj->getAllParticles().size();
            std::vector<Particle>& local = obj->getAllParticles();
#pragma omp for
            for (i = 0; i < amount; i++) {
                local[i].velocity =
                        local[i].velocity + dt * local[i].w * glm::vec3(0, -gravity, 0);
            }
            obj->afterVeloUpdate(dt);
#pragma omp for
            for (i = 0; i < amount; i++) {
                local[i].prevPos = local[i].getPosition();
                local[i].setPosition(local[i].getPosition() + dt * local[i].velocity);
            }

        }
    }

    /// 1.5 update map (spacialHash)
    globalHashMap.updateMap();
    /// 2. call the preConstraints function for all obj
    for (auto &obj:allObj) {
        obj->preConstraints(dt);
    }



    /// 3. generate dynamic constraints (like collision)

    /// 4. apply all constraints
    for (int k = 0; k < 3; k++) {
        for (int i = 0; i < allObj.size(); i++) {
            const auto &objPr = allObj[i]->getConstraints();
            for (int j = 0; j < objPr.size(); j++) {
                objPr[j]->apply(globalHashMap);
            }
        }
    }
    /// 5. call postConstraints
    for (auto &obj:allObj) {
        obj->postConstraints(dt);
    }

    /// 6. update position and correct velocity

    double length;

#pragma omp parrallel
    {

        for (auto &obj:allObj) {
            std::vector<Particle>& local = obj->getAllParticles();
            amount = obj->getAllParticles().size();
#pragma omp for
            for(i =0;i<amount;i++){
                assert(local[i].isValid() && "Error before velocity update");
                glm::f64vec3 pos(local[i].getPosition());
                glm::f64vec3 prevPos(local[i].prevPos);

                local[i].velocity = (pos - prevPos) / (double) dt;
                // limit velocity gain through "teleportation"*
                length = glm::length2( local[i].velocity);
                if (length > 500 * 500) {
                    local[i].velocity =  local[i].velocity / (float) (length / (500 * 500));
                }
                assert(local[i].isValid() && "Error during velocity update");
            }

//            for (auto &par:obj->getAllParticles()) {
//                assert(par.isValid() && "Error before velocity update");
//                glm::f64vec3 pos(par.getPosition());
//                glm::f64vec3 prevPos(par.prevPos);
//
//                par.velocity = (pos - prevPos) / (double) dt;
//                // limit velocity gain through "teleportation"
//                length = glm::length2(par.velocity);
//                if (length > 500 * 500) {
//                    par.velocity = par.velocity / (float) (length / (500 * 500));
//                }
//                assert(par.isValid() && "Error during velocity update");
//            }
        }
    }

    /// DEBUGING




//    int total = 0;
//    for (auto &obj:allObj) {
//        for (auto &par:obj->getAllParticles()) {
//            if (!par.isValid()) {
//                total++;
//            }
//        }
//    }
//    if (total > 0)
//        std::cout << "There are " << total << " invalid particles (containing nan)" << std::endl;
//
//
//    double totalLenght=0;
//    double curL;
//    u_int amount=0;
//    for (auto &obj:allObj) {
//        for (auto &par:obj->getAllParticles()) {
//            for (auto &obj2:allObj) {
//                for (auto &par2:obj2->getAllParticles()) {
//                    curL=glm::length2(par.getPosition()-par2.getPosition());
//                    if(std::isinf(curL) ){
//                        std::cerr << "curL is inf" <<std::endl;
//                    }
//                    totalLenght+=curL;
//                    amount++;
//                }
//            }
//        }
//    }






}

void ParticleSystem::drawOrInstance() const {
    for (int i = 0; i < allObj.size(); i++) {
        allObj[i]->drawOrAdd();
    }
}

void ParticleSystem::add(std::unique_ptr<PhysicsObject> obj) {
    allObj.push_back(std::move(obj));
}

ParticleSystem::ParticleSystem(Scene &sc) : scene(sc), globalHashMap(allObj, 2 * DEFAUTLT_PARTICLE_SIZE) {
    InstancedSphere::init();
    /// TODO temporary should be moved "into" defenition file
//    std::unique_ptr<Constraint> con(new AAInfPlaneConst(glm::vec3(0, 0, 0), 1));
//    constraints.push_back(std::move(con));
}


Simple::Simple(Scene &scene, glm::vec3 pos, float size, float weight) : PhysicsObject(scene), size(size) {
    allParticles.push_back(Particle(pos));
    allParticles[0].w = weight;
    allParticles[0].iw = 1.0f / weight;
    allParticles[0].radius = size;
    std::unique_ptr<Constraint> constraint(new DistanceConstraint(allParticles[0], 2 * size + DEFAUTLT_PARTICLE_SIZE));
    constraints.push_back(std::move(constraint));
}

bool Particle::isValid() {
    float val = position.x + position.y + position.z + velocity.x + velocity.y + velocity.z;
    return !(std::isnan(val) || std::isinf(val));
}

void Simple::afterVeloUpdate(float dt) {

}


void Simple::preConstraints(float dt) {

}

void Simple::postConstraints(float dt) {

}

void Simple::drawOrAdd() const {
    glm::mat4 model;
    model = glm::translate(model, allParticles[0].getPosition());
    model = glm::scale(model, glm::vec3(size));

    InstancedSphere::addInstance(model);
}


Wall::Wall(Scene &scene, int axis, glm::vec3 pos) : PhysicsObject(scene), plane(new Plane(pos, 20.0f)),
                                                    shader(scene.makeShader("regualV.glsl", "regularF.glsl")) {

    std::unique_ptr<Constraint> wall(new AAInfPlaneConst(pos, axis));
    constraints.push_back(std::move(wall));
    int absAxis = std::abs(axis) - 1; // get index corresponding to axis

    ori = glm::mat4(1.0f);
    if (absAxis == 0) {
        ori = glm::rotate(ori, glm::radians(270.0f), glm::vec3(0, 1, 0));
    } else if (absAxis == 1) {
        ori = glm::rotate(ori, glm::radians(90.0f), glm::vec3(1, 0, 0));
    }
//    else if(absAxis == 2){
//        ori = glm::rotate(ori,glm::radians(90.0f),glm::vec3(0,1,0));
//    }



}

void Wall::preConstraints(float dt) {

}

void Wall::postConstraints(float dt) {

}

void Wall::afterVeloUpdate(float dt) {

}

void Wall::drawOrAdd() const {
//    plane->render(shader,ori);
}

Box::Box(Scene &scene, glm::vec3 min, glm::vec3 max):PhysicsObject(scene) {
    std::unique_ptr<BoxCollision> box (new BoxCollision(min,max));
    constraints.push_back(std::move(box));

}

void Box::drawOrAdd() const {
    PhysicsObject::drawOrAdd();
}

void Box::afterVeloUpdate(float dt) {

}

void Box::preConstraints(float dt) {

}

void Box::postConstraints(float dt) {

}
