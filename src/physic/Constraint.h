//
// Created by knork on 7/3/17.
//

#ifndef FLUIDSIM_CONSTRAINT_H
#define FLUIDSIM_CONSTRAINT_H


#include <bits/unique_ptr.h>
#include <glm/vec3.hpp>
#include <vector>
#include <cmath>
#include <iostream>
#include "SpacialHash.h"


class PhysicsObject;

class Particle;

class Constraint {
public:
    virtual void apply(const SpacialHash &hashMap)=0;
};


/**
 * Constraint:
 * | fixed - any | -d >= 0
 *  Makes sure specified particles are at least distance(dist) away from each other.
 *  (Mostly collision)
 */
class DistanceConstraint : public Constraint {

    Particle &first;
    Particle *second;
    float distance;
public:
    DistanceConstraint(Particle &fixed, float dist) : first(fixed), distance(dist) {}

    DistanceConstraint(Particle &fixed, Particle &fixed2, float dist) : first(fixed),
                                                                        distance(dist) { second = &fixed2; }

    void apply(const SpacialHash &hashMap) override;
};


class AAInfPlaneConst : public Constraint {
    glm::vec3 pos;
    int axis;
    float distance = 0.5f;

public:
    /**
     *
     * @param position where the collision plane should be
     * @param axis 1 == normal of the plane is in the x direction, 2=y, 3=z
     */
    AAInfPlaneConst(glm::vec3 position, int axis) : pos(position), axis(axis) {
        assert(std::abs(axis) <= 3 && " Axis needs to be 1,2,3 or -1,-2,-3 any other value is not accepted");
    }

    void apply(const SpacialHash &hashMap) override;

};

class BoxCollision : public Constraint {


    glm::vec3 pMin,pMax;


public:
    BoxCollision(glm::vec3 pMin, glm::vec3 pMax):pMin(pMin),pMax(pMax){

    }

    void apply(const SpacialHash &hashMap) override ;


};


#endif //FLUIDSIM_CONSTRAINT_H
