//
// Created by knork on 7/25/17.
//

#ifndef FLUIDSIM_MYMARCHINGCUBE_H
#define FLUIDSIM_MYMARCHINGCUBE_H

#include <glm/vec3.hpp>
#include <vector>
#include "DrawableGeometry.h"

class Fluid;

class myMarchingCube {
public:
    myMarchingCube(const Fluid& f);
    ~ myMarchingCube(){
        delete isoValues;
    }

/**
 *
 * @param iso -  value at which the surface is to be drawn
 * @param pMin - min value of BoundingBox that the MC algo will cover
 * @param pMax  -  max value of the boundingBox that the MC algo will cover
 * @param resolution - how many intersection within the bounding box in each dimensions will be considered
 */
    std::vector<Vertex> updateIsoValues(float iso, glm::vec3 pMin, glm::vec3 pMax, glm::ivec3 resolution = {14, 33, 75});

    inline int gridToArray(glm::ivec3 grid){ return grid.x*res.y*res.z + grid.y*res.z+ grid.z;}

    inline glm::ivec3 arrayToGrid(int index){
        assert(index>=0);
        glm::ivec3 ret;
        int total = res.y*res.z;
        ret.x = index / (res.y*res.z);
        index = index - ret.x*total;
        ret.y = index / res.z;
        ret.z = index - ret.y * res.z;
        return ret;

    }

    inline glm::ivec3 worldToGrid(glm::vec3 world){
        assert(world.x >= pMin.x && world.y >= pMin.y && world.z >= pMin.z
        && world.x <= pMax.x && world.y <= pMax.y && world.z <= pMax.z);

        // TODO precompute

        float dX = (pMax.x-pMin.x) / res.x;
        float dY = (pMax.y-pMin.y) / res.y;
        float dZ = (pMax.z-pMin.z) / res.z;

        glm::ivec3 ret;
        ret.x = (int)(world.x / dX);
        ret.y = (int)(world.y / dY);
        ret.z = (int)(world.z / dZ);

        return ret;
    }



    inline glm::vec3 gridToWorld(size_t x, size_t y, size_t z){
        glm::vec3 ret;
        ret.x = pMin.x + (x* (pMax.x-pMin.x)/(res.x-1));
        ret.y = pMin.y + (y* (pMax.y-pMin.y)/(res.y-1));
        ret.z = pMin.z + (z* (pMax.z-pMin.z)/(res.z-1));
        return ret;
    }

    inline glm::vec3 gridToWorld(glm::ivec3 in){
        return gridToWorld(in.x,in.y,in.z);
    }



private:

    void addTriangle(std::vector<Vertex> *out, const char *edges, float *cube, glm::ivec3 curGridIndex, float iso);

    inline glm::vec3 interpolate(float isoMin, float isoMax, float iso, glm::vec3 minPos, glm::vec3 maxPos){
        float range = std::fabs(isoMax -isoMin);
        float mult = (-isoMin)/(isoMax-isoMin);
        glm::vec3 out= minPos+ mult*(maxPos-minPos);
        return out;
    }

    glm::ivec3 res;
    glm::vec3 pMin,pMax; // Bounding Box of what is to be processed by MC

    /// This is a 3D matrix of the isovalues in the processed cube
    float * isoValues = nullptr;
    const Fluid& fluid;
};


#endif //FLUIDSIM_MYMARCHINGCUBE_H
