//
// Created by knork on 6/21/17.
//



#include <fstream>

#include <glad/glad.h>
#include "Utils.h"
#include <iostream>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

Window *active_window;

Window::Window():Window(600,480) {

}
Window::Window(GLuint width, GLuint height):width(width),height(height){

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X

    // glfw window creation
    // --------------------
    const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    width = mode->width;
    height = mode->height;




    window = glfwCreateWindow(width, height, "Double Relaxation particle fluid Simulation", glfwGetPrimaryMonitor(), NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
    }

    //        // So gehts! Nicht anfassen!!!
//        screenWidth = 2560;
//        screenHeight = 1440;
//        glfwWindowHint(GLFW_DECORATED, 0);
//        //  glfwGetMonitors(&count)[0]
//        GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "ImGui OpenGL3 example",glfwGetPrimaryMonitor() ,NULL);

    glfwMakeContextCurrent(window);
//    glfwSetFramebufferSizeCallback(window, size_changed);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);
    glfwSetInputMode(window, GLFW_CURSOR,GLFW_CURSOR_DISABLED);

    // TODO pull into Scene

    active_window = this;

    deltaTime = lastFrame = 0;

    camera.setUpBuffers();


}
bool Window::shoudlClose() const{
    return glfwWindowShouldClose(window);
}

void Window::update() {
    float currentFrame = glfwGetTime();
    if(lastFrame>0.0f )
        deltaTime = currentFrame - lastFrame;

    lastFrame = currentFrame;
    process_inputs();
    glfwSwapBuffers(window);
    glfwPollEvents();
}

Window::~Window() {
    glfwDestroyWindow(window); // deletes pointer to window
    glfwTerminate();
}

float Window::getDt() {
    if(paused)
        return 0;
    return deltaTime/curTimeCo;
}

float Window::getDtUnpausable() {
    return deltaTime;
}

void Window::process_inputs()
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS){
        float cur=glfwGetTime();
        if(cur-lastKey>0.2) {
            paused = !paused;
            lastKey=cur;
        }
    }
    if (glfwGetKey(window, GLFW_KEY_COMMA) == GLFW_PRESS) {
        float cur=glfwGetTime();
        if(cur-lastKey>0.2) {
            slowMo = !slowMo;
            if (slowMo) {
                curTimeCo = slow;
            } else {
                curTimeCo = normal;
            }
            lastKey=cur;
        }
    }

    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS){
        float cur=glfwGetTime();
        if(cur-lastKey>0.2) {
            if(camera.MovementSpeed>40)
                camera.MovementSpeed= 10;
            else
                camera.MovementSpeed = 45;
            lastKey=cur;
        }
    }
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.ProcessKeyboard(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.ProcessKeyboard(RIGHT, deltaTime);
}

void Window::size_changed(int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    lastX=width/2;
    lastY=height/2;
    firstMouse=true;

    this->width = width;
    this->height = height;

    glViewport(0, 0, width, height);
}


void Window::mouse(double xpos, double ypos)
{
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
        return;
    }
    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;
    camera.ProcessMouseMovement(xoffset, yoffset);
}


void Window::scroll(double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(yoffset);
}

/// Pull global callbacks into class hirachy


void framebuffer_size_callback(GLFWwindow* window, int width, int height){
    active_window->size_changed(width, height);
}
void mouse_callback(GLFWwindow* window, double xpos, double ypos){
    active_window->mouse(xpos, ypos);
}
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset){
    active_window->scroll(yoffset, 0);
}
