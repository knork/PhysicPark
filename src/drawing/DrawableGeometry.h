//
// Created by knork on 6/21/17.
//

#ifndef FLUIDSIM_GEOMETRY_H
#define FLUIDSIM_GEOMETRY_H
#include <glm/glm.hpp>

#include <vector>
#include "Utils.h"
#include "../physic/PhysicGeometry.h"


/**
 * Sturct for transfer to the GPU, universal alignement
 */
struct Vertex {
    // Position
    glm::vec3 Position;
    // Normal
    glm::vec3 Normal;
    // TexCoords
//    glm::vec2 TexCoords;

    Vertex():Position(0,0,0),Normal(0,0,0){
    }
    Vertex(glm::vec3 pos, glm::vec3 normal, glm::vec2 tex):
            Position(pos),Normal(normal){}

    Vertex(float x, float y, float z, float nx, float ny, float nz):
            Position(x,y,z),Normal(nx,ny,nz){}
//    glm::vec3 Tangent;
};

/**
 * Represents an object in the scene that is only
 * drawable but can not interact with other objects.
 * However they can be attached PhysicsObjects to
 * give the PhysicObject a visual representation.
 */

class DrawableObj:public SceneObj{
protected:
    GLuint VAO, VBO, EBO;


    std::vector<GLuint> indices;
    std::vector<Vertex> vertices; // in obj-space

    static std::vector<Shader *> shader;
    static std::vector<std::vector<glm::mat4>> model;


     /**
     * Expects vertices-array to be filled.
     * If indices array is filled it will also be pushed to GPU
     */
    virtual void initBuffers();

public:
    glm::vec4 color;
    DrawableObj(glm::vec3 pos) : SceneObj(pos){}
    virtual ~ DrawableObj();
    virtual void render(Shader &shader) const =0;
    virtual void render(Shader &shader,glm::mat4 model) const =0;

};




class Sphere : public DrawableObj {
public:
//    Sphere(glm::vec3 pos, glm::vec3 vel):RigidBody(pos,vel){}
    Sphere(glm::vec3 pos, int res);
    ~Sphere(){};

    void render(Shader &shader) const override;
    void render(Shader &shader,glm::mat4 model) const override;
protected:
//    void initBuffers() override;
private:
    void initSolidSphere(int resolution);
    void refineSolidSphere(const std::vector<glm::vec3>& sphere,std::vector<glm::vec3>& refined);
};

class Cube : public DrawableObj{
public:
    Cube(glm::vec3 pos);
    ~Cube() override;

    void render(Shader &shader) const override;
    void render(Shader &shader,glm::mat4 model) const override;
protected:
    void initBuffers() override;
};


class Plane: public DrawableObj{
public:
    Plane(glm::vec3 pos, float extend);

    void render(Shader &shader) const override;

    void render(Shader &shader, glm::mat4 model) const override;
};

class Mesh:public DrawableObj{


public:
    void render(Shader &shader) const override;

public:
    Mesh( std::vector<Vertex> &vertices, std::vector<GLuint> &indices);
    ~Mesh();

    void render(Shader &shader, glm::mat4 model) const override;
};


#endif //FLUIDSIM_GEOMETRY_H
