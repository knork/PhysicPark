//
// Created by knork on 7/25/17.
//

#include "myMarchingCube.h"
#include "../physic/Fluid.h"
#include "LookUp.h"


myMarchingCube::myMarchingCube(const Fluid &f) : fluid(f) {

}

std::vector<Vertex> myMarchingCube::updateIsoValues(float iso, glm::vec3 pMin, glm::vec3 pMax, glm::ivec3 resolution) {

    // Do we need to reallocate memory?
    if (resolution != this->res) {
        if (isoValues)
            delete isoValues;

        res=resolution;
        int total = resolution.x * resolution.y * resolution.z;
        isoValues = new float[total];
    }
//    this->pMin=pMin-3.0f*glm::vec3(1,1,1);
//    this->pMax=pMax+3.0f*glm::vec3(1,1,1);
//
    this->pMin=pMin;
    this->pMax=pMax;

    /// Fill 3D grid with the iso values at each position


    size_t i, j, k,index;
    glm::vec3 world;





    int total = res.x*res.y*res.z;
// introduces "some" artifacts
    #pragma omp parallel for private(world)
    for (i = 0; i < total; i++) {
                world = gridToWorld(arrayToGrid(i));
                isoValues[i] = fluid.getDensity(world);
                assert(!std::isnan(isoValues[i]));
    }

    // Go through the Grid and add the vertices and indices accordingly

    float cube[8]; // the 8 isovalues for the current cube
    u_char  lut;


    std::vector<Vertex> ret;
    for (i = 0; i < res.x - 1; i++) {
        for (j = 0; j < res.y - 1; j++) {
            for (k = 0; k < res.z - 1; k++) {
                // move the

                lut=0;
                for (int p = 0; p < 8; ++p) {
                    index = gridToArray(glm::ivec3(i + ((p ^ (p >> 1)) & 1), j + ((p >> 1) & 1), k + ((p >> 2) & 1)));
                    cube[p] = isoValues[index]-iso;
                    if (cube[p] > 0){
                        lut += (1 << p);
                    }
                }
                // get **edges** where our vertices pass through
                char nt = 0;
                while (casesClassic[lut][3 * nt] != -1) {
                    addTriangle(&ret, &casesClassic[lut][3 * nt], cube, glm::ivec3(i,j,k), iso);
                    nt++;
                }
            }
        }
    }

    return std::move(ret);

}

void
myMarchingCube::addTriangle(std::vector<Vertex> *out, const char *edges, float *cube, glm::ivec3 curGridIndex,
                            float iso) {

    assert(edges[0]!=-1);

    // ok we know on what edge each of the vertices lies on.

        glm::vec3 tv[3];
        for (int t = 0; t < 3; ++t) {
            switch (edges[t]) {
                case 0 :
                    tv[t] = interpolate(cube[0], cube[1], iso,
                                        gridToWorld(curGridIndex.x, curGridIndex.y, curGridIndex.z),
                                        gridToWorld(curGridIndex.x + 1, curGridIndex.y, curGridIndex.z));
                    break;
                case 1 :
                    tv[t] = interpolate(cube[1], cube[2], iso,
                                        gridToWorld(curGridIndex.x + 1, curGridIndex.y, curGridIndex.z),
                                        gridToWorld(curGridIndex.x + 1, curGridIndex.y + 1, curGridIndex.z));
                    break;
                case 2 :
                    tv[t] = interpolate(cube[2], cube[3], iso,
                                        gridToWorld(curGridIndex.x + 1, curGridIndex.y + 1, curGridIndex.z),
                                        gridToWorld(curGridIndex.x, curGridIndex.y + 1, curGridIndex.z));
                    break;
                case 3 :
                    tv[t] = interpolate(cube[3], cube[0], iso,
                                        gridToWorld(curGridIndex.x, curGridIndex.y + 1, curGridIndex.z),
                                        gridToWorld(curGridIndex.x, curGridIndex.y, curGridIndex.z));
                    break;

                case 4 :
                    tv[t] = interpolate(cube[4], cube[5], iso,
                                        gridToWorld(curGridIndex.x, curGridIndex.y, curGridIndex.z + 1),
                                        gridToWorld(curGridIndex.x + 1, curGridIndex.y, curGridIndex.z + 1));
                    break;
                case 5 :
                    tv[t] = interpolate(cube[5], cube[6], iso,
                                        gridToWorld(curGridIndex.x + 1, curGridIndex.y, curGridIndex.z + 1),
                                        gridToWorld(curGridIndex.x + 1, curGridIndex.y + 1, curGridIndex.z + 1));
                    break;
                case 6 :
                    tv[t] = interpolate(cube[6], cube[7], iso,
                                        gridToWorld(curGridIndex.x + 1, curGridIndex.y + 1, curGridIndex.z + 1),
                                        gridToWorld(curGridIndex.x, curGridIndex.y + 1, curGridIndex.z + 1));
                    break;
                case 7 :
                    tv[t] = interpolate(cube[7], cube[4], iso,
                                        gridToWorld(curGridIndex.x, curGridIndex.y + 1, curGridIndex.z + 1),
                                        gridToWorld(curGridIndex.x, curGridIndex.y, curGridIndex.z + 1));
                    break;
                case 8 :
                    tv[t] = interpolate(cube[0], cube[4], iso,
                                        gridToWorld(curGridIndex.x, curGridIndex.y, curGridIndex.z + 1),
                                        gridToWorld(curGridIndex.x , curGridIndex.y, curGridIndex.z ));
                    break;
                case 9 :
                    tv[t] = interpolate(cube[1], cube[5], iso,
                                        gridToWorld(curGridIndex.x + 1, curGridIndex.y, curGridIndex.z + 1),
                                        gridToWorld(curGridIndex.x + 1, curGridIndex.y , curGridIndex.z ));
                    break;
                case 10 :
                    tv[t] = interpolate(cube[2], cube[6], iso,
                                        gridToWorld(curGridIndex.x + 1, curGridIndex.y + 1, curGridIndex.z + 1),
                                        gridToWorld(curGridIndex.x+1, curGridIndex.y + 1, curGridIndex.z ));
                    break;
                case 11 :
                    tv[t] = interpolate(cube[3], cube[7], iso,
                                        gridToWorld(curGridIndex.x, curGridIndex.y + 1, curGridIndex.z + 1),
                                        gridToWorld(curGridIndex.x, curGridIndex.y+1, curGridIndex.z ));
                    break;
                default :
                    std::cerr << "Marching Cubes: invalid triangle " << "\n";
                    break;
            }

            if (edges[t] == -1) {
                std::cerr << "Marching Cubes: invalid triangle " << "\n";
                //print_cube() ;
            }


        }

    glm::vec3 normal= glm::cross(tv[0]-tv[2],tv[0]-tv[1]);
    out->push_back({tv[0],normal,{0,0}});
    out->push_back({tv[1],normal,{0,0}});
    out->push_back({tv[2],normal,{0,0}});

    }

