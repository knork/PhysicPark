//
// Created by knork on 6/28/17.
//

#include <vector>
#include "Scene.h"
#include "InstancedSphere.h"

void Scene::setKeyBoardCallback(std::function<void()> func) {
    additionalKeyboardSignals=func;
}

Scene::~Scene() {
    InstancedSphere::del();
}

Scene::Scene(Window& win):win(win),camera(win.camera),particleSystem(ParticleSystem(*this)) {
    camera.Position =(glm::vec3(-3.12f, 15.0f, 30.0f));
//    win.camera=camera;
    camera.Yaw = -57;
    camera.updateCameraVectors();
    assert(&win.camera==&camera);
}

void Scene::update(float dt) {
    additionalKeyboardSignals();
    particleSystem.update(dt);
    assert(lights && "No Lights where added to the Scene");
}

void Scene::render() const {
    /// Update all shader values (camera and lights)
    glm::mat4 perspective = glm::perspective(glm::radians(camera.Zoom), (float)win.width / (float)win.height, 0.1f, 10000.0f);
    camera.updateUniBuffer(perspective); // set camera matrices for all shader
    lights->updateUniBuffer(); // set light parameters for all shader

    for (auto &o:drawableObjects) {
        std::get<1>(o)->render(*allShader[std::get<0>(o)]);
    }

    // To render spheres for particle Obj
    InstancedSphere::resetInstances();
    InstancedSphere::shader->use();
    // give each ParticleObject the chance to add sphere instances or
    // draw their own shape
    particleSystem.drawOrInstance();
    InstancedSphere::draw();
}

size_t Scene::getShaderIndex(Shader &shader) {
    if (lastShaderIndex > -1) {
        if (allShader[lastShaderIndex].get() == &shader) {
            return lastShaderIndex;
        }
    }
    for (int i = 0; i < allShader.size(); i++) {
        if (allShader[i].get() == &shader) {
            return i;
        }
    }
    static_assert(true,
                  "Shader not found in allShader-array. Thus it must have been externally initialized, default to first Shader");
    return 0;

}

void Scene::add(std::unique_ptr<PhysicsObject> obj) {
    particleSystem.add(std::move(obj));

}

void Scene::add(Shader &shader, std::unique_ptr<DrawableObj> obj) {
    int i = getShaderIndex(shader);
    drawableObjects.push_back(std::make_tuple(i,std::move(obj)));
}

void Scene::set(std::unique_ptr<LightCollection> light) {
    lights = std::move(light);
}

Shader &Scene::makeShader(const char *vertexPath, const char *fragmentPath, const char *geometryPath) {
    std::string total =
            std::string(vertexPath) + std::string(fragmentPath) + (geometryPath ? std::string(geometryPath) : "");

    for (int i = 0; i < shaderPaths.size(); i++) {
        if (shaderPaths[i].compare(total) == 0) {
            return *allShader[i];
        }
    }
    // if shader has not yet been used create a new one an return the refrence
    std::unique_ptr<Shader> ptr(new Shader(vertexPath, fragmentPath, geometryPath));
    allShader.push_back(std::move(ptr));
    Shader * shrPointer = allShader[allShader.size() - 1].get();
    return *shrPointer;
}

Shader &Scene::getShader(int index) {
    return *allShader[index];
}


void LightCollection::attachLight(SceneObj *in) {

    allLights.push_back({in});
}


void LightCollection::initBuffer() {
    glGenBuffers(1, &uboMatrices);
    glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
    glBufferData(GL_UNIFORM_BUFFER, lightBufferSize, NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferRange(GL_UNIFORM_BUFFER, bufferIndex, uboMatrices, 0, lightBufferSize * sizeof(GLbyte));
}

void LightCollection::updateUniBuffer() const {

    curLightStructs.clear();
    for(auto & in:allLights) {
        curLightStructs.push_back(GLSL_Lights(in->getPosition()));
    }

    glm::uint amount = allLights.size();
    if (amount >= 30)
        amount = 30;

    glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
    // offset is 16 ... why?!? dunno
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::uint), &(amount));
    glBufferSubData(GL_UNIFORM_BUFFER, 16, amount * sizeof(GLSL_Lights), &curLightStructs[0]);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

}

LightCollection::LightCollection() {
    initBuffer();
}
