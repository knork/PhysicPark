//
// Created by knork on 6/28/17.
//

#ifndef FLUIDSIM_SCENE_H
#define FLUIDSIM_SCENE_H


#include "Utils.h"
#include "../physic/PhysicGeometry.h"
#include "DrawableGeometry.h"
#include <vector>
#include <bits/unique_ptr.h>
#include <functional>

/**
 * Collects all the lights and their information in the system.
 * Sends them as one big uniform buffer data-stream to the
 * gpu making them availible in all Shaders.
 *
 * GL_UNIFORM_BUFFER index 0
 */
class LightCollection{

    // struct with the same padding as glsl std140 layout
    struct GLSL_Lights {
        glm::vec3 ambient; // 12 Byte
        glm::float32 padding; // 4 Bytes
        // 16
        glm::vec3 pos;// 12 Bytes
        glm::float32 padding2; // 4 Bytes
        // 32
        glm::uint type; // 4 Bytes
        glm::float32 spec; // 4 Bytes
        glm::float32 padding3; // 4 Bytes
        glm::float32 padding4; // 4 Bytes
        // Total : 48 Bytes
        GLSL_Lights(glm::vec3 position):ambient(glm::vec3(0)),pos(position),type(0),spec(0){}
    };

    // working with max of 30 lights and an additional int to pass the actual number of lights
    // max for intelGPU is 16KB thus max radius of lights is ~ 340
    const GLuint lightBufferSize = 30 * 48 + 16;
    const GLuint bufferIndex = 1;

    GLuint uboMatrices;
    mutable std::vector<GLSL_Lights> curLightStructs;
    std::vector<SceneObj*> allLights;
    void initBuffer();




public:
    LightCollection();
    /// Lights need to be attached to discrete Scene Object
    /// which has a position assiciated with it
    void attachLight(SceneObj* in);
    /// call before draw -> update lights information in buffer
    void updateUniBuffer()const;
};

/**
 * Manages all activities in a scene.
 *
 * - Shaders are created and stored in this class.
 * This allows us to reduce the amount of openGL call.
 * All DrawableObjects should assume that their shader
 * has already been activated for them. In order to
 * achieve this we have to keep track of what object
 * uses what shaders
 *
 * - Keeps reference to camera to update view-matrix
 * before objects draw themself.
 *
 * - Keeps refrence to windows to update perspective
 * matrix befor objects draw themself.
 */
class Scene {


    std::function<void()> additionalKeyboardSignals;

    /// make sure to not compile same shaders twice
    std::vector<std::string> shaderPaths;
    std::vector<std::unique_ptr<Shader>> allShader;

    /// allows us to alter camera setting from scene-describtion
    Camera& camera;
    /// We need the screen dimensions, as well as the window-pointer (for gui)
    Window& win;

    /// stores all objects that are only drawn but not animated/collision relevant (static lights and such)
    std::vector<std::tuple<size_t,std::unique_ptr<DrawableObj>>> drawableObjects;

    std::unique_ptr<LightCollection> lights;
    ParticleSystem particleSystem;

    // keep track of last Shader used, since next obj propbably going to use the same one
    int lastShaderIndex= -1;
    // maps inserted shader path to index into the shader array
    size_t getShaderIndex(Shader &shader);


public:

    explicit Scene(Window& win);
    Scene(const Scene &)= delete;
    Scene(Scene &&)= default;
    Scene& operator=(const Scene& other) = delete;
    ~Scene();

    void update(float dt);
    void render()const;

    /**
     * Adds a @ParticleObject to the scene.
     *
     * @param shader Shader the object is to be drawn with
     * @param obj use std::move to transfer ownership to the scene.
     * Also you can use the typedefs from @SceneDefinitions
     */
    void add(std::unique_ptr<PhysicsObject> obj);
    /**
     * Adds a @DrawableObject to the scene
     *
     * @param shader Shader the object is to be drawn with
     * @param obj use std::move to transfer ownership to the scene.
     * Also you can use the typedefs from @SceneDefinitions
     */
    void add(Shader &shader, std::unique_ptr<DrawableObj> obj);

    void set(std::unique_ptr<LightCollection> light);

    /** Makes sure that all shaders within one scene are unique
     * @param vertexPath - Path the vertex Shader file
     * @param fragmentPath - Path the fragment Shader file
     * @param geometryPath - Path the geometry Shader file
     * @return shader refrence to be used to instanciate DrawableObjects or
     * ParticleObjects
     */
    Shader& makeShader(const char* vertexPath, const char* fragmentPath, const char* geometryPath=nullptr);

    /**
     * Since we need to keep track of which Object uses which shader.
     * Each shader is given a index. We can get the Shader that corresponds
     * to a given index with this function.
     */
    Shader& getShader(int index);

    void setKeyBoardCallback(std::function<void()> func);

    Window& getWindow(){return win;}
};




#endif //FLUIDSIM_SCENE_H
