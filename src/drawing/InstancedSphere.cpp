//
// Created by knork on 7/6/17.
//

#include <glm/vec3.hpp>
#include "InstancedSphere.h"


GLuint InstancedSphere::VAO,InstancedSphere::VBO,InstancedSphere::EBO,InstancedSphere::buffer;

std::vector<GLuint> InstancedSphere::indices;
std::vector<Vertex> InstancedSphere::vertices; // in obj-space

std::vector<glm::mat4> InstancedSphere::modelMAtrices;
std::unique_ptr<Shader> InstancedSphere::shader;

void InstancedSphere::refineSphere(const std::vector<glm::vec3>& sphere,std::vector<glm::vec3>& refined) {
    for(size_t i=0;i<sphere.size()/3;i++) {
        const glm::vec3& a = sphere[3*i+0];
        const glm::vec3& b = sphere[3*i+1];
        const glm::vec3& c = sphere[3*i+2];

        glm::vec3 ab = a+b;
        glm::vec3 bc = b+c;
        glm::vec3 ca = c+a;


        ab = glm::normalize(ab);
        bc = glm::normalize(bc);
        ca = glm::normalize(ca);

        refined.push_back(a);
        refined.push_back(ab);
        refined.push_back(ca);

        refined.push_back(ab);
        refined.push_back(b);
        refined.push_back(bc);

        refined.push_back(bc);
        refined.push_back(c);
        refined.push_back(ca);

        refined.push_back(ab);
        refined.push_back(bc);
        refined.push_back(ca);
    }
}

void InstancedSphere::init() {

    int resolution = 2;

    std::vector<glm::vec3> ico;
    glm::float32 gr = 0.5*(1.0+sqrt(5.0));

    ico.push_back( glm::vec3(gr,1.0,0.0));
    ico.push_back( glm::vec3(1.0,0.0,gr));
    ico.push_back( glm::vec3(gr,-1.0,0.0));

    ico.push_back( glm::vec3(gr,1.0,0.0));
    ico.push_back( glm::vec3(gr,-1.0,0.0));
    ico.push_back( glm::vec3(1.0,0.0,-gr));

    ico.push_back( glm::vec3(gr,1.0,0.0));
    ico.push_back( glm::vec3(0.0,gr,-1.0));
    ico.push_back( glm::vec3(0.0,gr,1.0));

    ico.push_back( glm::vec3(gr,1.0,0.0));
    ico.push_back( glm::vec3(0.0,gr,1.0));
    ico.push_back( glm::vec3(1.0,0.0,gr));

    ico.push_back( glm::vec3(gr,1.0,0.0));
    ico.push_back( glm::vec3(1.0,0.0,-gr));
    ico.push_back( glm::vec3(0.0,gr,-1.0));

    ico.push_back( glm::vec3(-gr,-1.0,0.0));
    ico.push_back( glm::vec3(-1.0,0.0,gr));
    ico.push_back( glm::vec3(-gr,1.0,0.0));

    ico.push_back( glm::vec3(-gr,-1.0,0.0));
    ico.push_back( glm::vec3(-gr,1.0,0.0));
    ico.push_back( glm::vec3(-1.0,0.0,-gr));

    ico.push_back( glm::vec3(-gr,-1.0,0.0));
    ico.push_back( glm::vec3(0.0,-gr,-1.0));
    ico.push_back( glm::vec3(0.0,-gr,1.0));

    ico.push_back( glm::vec3(-gr,-1.0,0.0));
    ico.push_back( glm::vec3(0.0,-gr,1.0));
    ico.push_back( glm::vec3(-1.0,0.0,gr));

    ico.push_back( glm::vec3(-gr,-1.0,0.0));
    ico.push_back( glm::vec3(-1.0,0.0,-gr));
    ico.push_back( glm::vec3(0.0,-gr,-1.0));

    ico.push_back( glm::vec3(1.0,0.0,gr));
    ico.push_back( glm::vec3(-1.0,0.0,gr));
    ico.push_back( glm::vec3(0.0,-gr,1.0));

    ico.push_back( glm::vec3(1.0,0.0,gr));
    ico.push_back( glm::vec3(0.0,gr,1.0));
    ico.push_back( glm::vec3(-1.0,0.0,gr));

    ico.push_back( glm::vec3(0.0,gr,1.0));
    ico.push_back( glm::vec3(-gr,1.0,0.0));
    ico.push_back( glm::vec3(-1.0,0.0,gr));

    ico.push_back( glm::vec3(0.0,gr,1.0));
    ico.push_back( glm::vec3(0.0,gr,-1.0));
    ico.push_back( glm::vec3(-gr,1.0,0.0));

    ico.push_back( glm::vec3(0.0,gr,-1.0));
    ico.push_back( glm::vec3(-1.0,0.0,-gr));
    ico.push_back( glm::vec3(-gr,1.0,0.0));

    ico.push_back( glm::vec3(-1.0,0.0,-gr));
    ico.push_back( glm::vec3(0.0,gr,-1.0));
    ico.push_back( glm::vec3(1.0,0.0,-gr));

    ico.push_back( glm::vec3(-1.0,0.0,-gr));
    ico.push_back( glm::vec3(1.0,0.0,-gr));
    ico.push_back( glm::vec3(0.0,-gr,-1.0));

    ico.push_back( glm::vec3(0.0,-gr,-1.0));
    ico.push_back( glm::vec3(1.0,0.0,-gr));
    ico.push_back( glm::vec3(gr,-1.0,0.0));

    ico.push_back( glm::vec3(0.0,-gr,-1.0));
    ico.push_back( glm::vec3(gr,-1.0,0.0));
    ico.push_back( glm::vec3(0.0,-gr,1.0));

    ico.push_back( glm::vec3(0.0,-gr,1.0));
    ico.push_back( glm::vec3(gr,-1.0,0.0));
    ico.push_back( glm::vec3(1.0,0.0,gr));



    for(size_t i=0;i<ico.size();i++)
        ico[i] = glm::normalize(ico[i]);

    for(int i=0;i<resolution;i++) {
        std::vector<glm::vec3> ico_refined;
        InstancedSphere::refineSphere(ico,ico_refined);
        ico = ico_refined;
    }

    std::vector<glm::vec3> vertexWithNormal;
    GLuint id;


    for(size_t i=0;i<ico.size();i++) {
        vertices.push_back({ico[i],ico[i],glm::vec2{0,0}});
    }

    std::unique_ptr<Shader>some(new Shader("instancedV.glsl","instancedF.glsl"));
    shader = std::move(some);

    // Vertex Buffer Object
//    GLuint buffer;


    glBindVertexArray(VAO);
    glDeleteBuffers(1, &buffer);
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);

    glBufferData(GL_ARRAY_BUFFER, modelMAtrices.size() * sizeof(glm::mat4), glm::value_ptr(modelMAtrices[0]),
                     GL_DYNAMIC_DRAW);

    // Create buffers/arrays
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);


    glBindVertexArray(VAO);
    // Load data into vertex buffers
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // A great thing about structs is that their memory layout is sequential for all its items.
    // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
    // again translates to 3/2 floats which translates to a byte array.
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    // Set the vertex attribute pointers
    // Vertex Positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *) 0);
    // Vertex Normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *) offsetof(Vertex, Normal));
    // Vertex Texture Coords
//    glEnableVertexAttribArray(2);
//    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *) offsetof(Vertex, TexCoords));
    // 3,4,5,6 is used by the instance matrix

    // Vertex Attributes
    std::size_t vec4Size = sizeof(glm::vec4);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (GLvoid *) 0);
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (GLvoid *) (vec4Size));
    glEnableVertexAttribArray(5);
    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (GLvoid *) (2 * vec4Size));
    glEnableVertexAttribArray(6);
    glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (GLvoid *) (3 * vec4Size));

    glVertexAttribDivisor(3, 1);
    glVertexAttribDivisor(4, 1);
    glVertexAttribDivisor(5, 1);
    glVertexAttribDivisor(6, 1);

    glBindVertexArray(0);

}

void InstancedSphere::addInstance(glm::mat4 model) {
    modelMAtrices.push_back(model);
}

void InstancedSphere::resetInstances() {
    modelMAtrices.clear();
}

void InstancedSphere::draw() {
    shader->use();


    glBindVertexArray(VAO);
    setUpModel();


//    glDrawElementsInstanced(GL_TRIANGLES, indices.radius(), GL_UNSIGNED_INT, 0, modelMAtrices.radius());
    glDrawArraysInstanced(GL_TRIANGLES,0,vertices.size(),modelMAtrices.size());
//    glDrawArrays(GL_TRIANGLES,0,vertices.radius());

    glBindVertexArray(0);
}

void InstancedSphere::setUpModel() {

    glDeleteBuffers(1, &buffer);
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);

        glBufferData(GL_ARRAY_BUFFER, modelMAtrices.size() * sizeof(glm::mat4), glm::value_ptr(modelMAtrices[0]),
                     GL_DYNAMIC_DRAW);

    // Vertex Attributes
    std::size_t vec4Size = sizeof(glm::vec4);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (GLvoid *) 0);
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (GLvoid *) (vec4Size));
    glEnableVertexAttribArray(5);
    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (GLvoid *) (2 * vec4Size));
    glEnableVertexAttribArray(6);
    glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (GLvoid *) (3 * vec4Size));

    glVertexAttribDivisor(3, 1);
    glVertexAttribDivisor(4, 1);
    glVertexAttribDivisor(5, 1);
    glVertexAttribDivisor(6, 1);



}

void InstancedSphere::del() {
    glDeleteVertexArrays(1,&VAO);
    glDeleteBuffers(1,&VBO);
    glDeleteBuffers(1,&buffer);

}
