//
// Created by knork on 7/6/17.
//

#ifndef FLUIDSIM_INSTANCEDSPHERE_H
#define FLUIDSIM_INSTANCEDSPHERE_H


#include <glad/glad.h>
#include "DrawableGeometry.h"

class InstancedSphere {

    static GLuint VAO, VBO, EBO, buffer;


    static std::vector<GLuint> indices;
    static std::vector<Vertex> vertices; // in obj-space

    static std::vector<glm::mat4> modelMAtrices;

    static void refineSphere(const std::vector<glm::vec3>& sphere,std::vector<glm::vec3>& refined);
    static  void setUpModel();
public:
    static std::unique_ptr<Shader> shader;
    static void init();
    static void resetInstances();
    static void addInstance(glm::mat4 model);
    static void draw();
    static void del();
};


#endif //FLUIDSIM_INSTANCEDSPHERE_H
