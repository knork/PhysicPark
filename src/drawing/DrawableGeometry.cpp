//
// Created by knork on 6/21/17.
//

#include "DrawableGeometry.h"



DrawableObj::~DrawableObj(){
    glDeleteVertexArrays(1,&VAO);
    glDeleteBuffers(1,&VBO);
}

void DrawableObj::initBuffers() {

    // Create buffers/arrays
    glGenVertexArrays(1, &this->VAO);
    glGenBuffers(1, &this->VBO);
//    glGenBuffers(1, &this->EBO);

    glBindVertexArray(this->VAO);
    // Load data into vertex buffers
    glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
    // A great thing about structs is that their memory layout is sequential for all its items.
    // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
    // again translates to 3/2 floats which translates to a byte array.
    glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);

    if(indices.size()>0) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);
    }

    // Set the vertex attribute pointers
    // Vertex Positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
    // Vertex Normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Normal));
//    // Vertex Texture Coords
//    glEnableVertexAttribArray(2);
//    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));

//    // put it on 7 to keep consistent with vInstance glsl
//    glEnableVertexAttribArray(7);
//    glVertexAttribPointer(7, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid *) offsetof(Vertex, Tangent));

    glBindVertexArray(0);

}


void Sphere::refineSolidSphere(const std::vector<glm::vec3>& sphere,std::vector<glm::vec3>& refined) {
    for(size_t i=0;i<sphere.size()/3;i++) {
        const glm::vec3& a = sphere[3*i+0];
        const glm::vec3& b = sphere[3*i+1];
        const glm::vec3& c = sphere[3*i+2];

        glm::vec3 ab = a+b;
        glm::vec3 bc = b+c;
        glm::vec3 ca = c+a;


        ab = glm::normalize(ab);
        bc = glm::normalize(bc);
        ca = glm::normalize(ca);

        refined.push_back(a);
        refined.push_back(ab);
        refined.push_back(ca);

        refined.push_back(ab);
        refined.push_back(b);
        refined.push_back(bc);

        refined.push_back(bc);
        refined.push_back(c);
        refined.push_back(ca);

        refined.push_back(ab);
        refined.push_back(bc);
        refined.push_back(ca);
    }
}

void Sphere::initSolidSphere(int resolution) {
    std::vector<glm::vec3> ico;
    glm::float32 gr = 0.5*(1.0+sqrt(5.0));

    ico.push_back( glm::vec3(gr,1.0,0.0));
    ico.push_back( glm::vec3(1.0,0.0,gr));
    ico.push_back( glm::vec3(gr,-1.0,0.0));

    ico.push_back( glm::vec3(gr,1.0,0.0));
    ico.push_back( glm::vec3(gr,-1.0,0.0));
    ico.push_back( glm::vec3(1.0,0.0,-gr));

    ico.push_back( glm::vec3(gr,1.0,0.0));
    ico.push_back( glm::vec3(0.0,gr,-1.0));
    ico.push_back( glm::vec3(0.0,gr,1.0));

    ico.push_back( glm::vec3(gr,1.0,0.0));
    ico.push_back( glm::vec3(0.0,gr,1.0));
    ico.push_back( glm::vec3(1.0,0.0,gr));

    ico.push_back( glm::vec3(gr,1.0,0.0));
    ico.push_back( glm::vec3(1.0,0.0,-gr));
    ico.push_back( glm::vec3(0.0,gr,-1.0));

    ico.push_back( glm::vec3(-gr,-1.0,0.0));
    ico.push_back( glm::vec3(-1.0,0.0,gr));
    ico.push_back( glm::vec3(-gr,1.0,0.0));

    ico.push_back( glm::vec3(-gr,-1.0,0.0));
    ico.push_back( glm::vec3(-gr,1.0,0.0));
    ico.push_back( glm::vec3(-1.0,0.0,-gr));

    ico.push_back( glm::vec3(-gr,-1.0,0.0));
    ico.push_back( glm::vec3(0.0,-gr,-1.0));
    ico.push_back( glm::vec3(0.0,-gr,1.0));

    ico.push_back( glm::vec3(-gr,-1.0,0.0));
    ico.push_back( glm::vec3(0.0,-gr,1.0));
    ico.push_back( glm::vec3(-1.0,0.0,gr));

    ico.push_back( glm::vec3(-gr,-1.0,0.0));
    ico.push_back( glm::vec3(-1.0,0.0,-gr));
    ico.push_back( glm::vec3(0.0,-gr,-1.0));

    ico.push_back( glm::vec3(1.0,0.0,gr));
    ico.push_back( glm::vec3(-1.0,0.0,gr));
    ico.push_back( glm::vec3(0.0,-gr,1.0));

    ico.push_back( glm::vec3(1.0,0.0,gr));
    ico.push_back( glm::vec3(0.0,gr,1.0));
    ico.push_back( glm::vec3(-1.0,0.0,gr));

    ico.push_back( glm::vec3(0.0,gr,1.0));
    ico.push_back( glm::vec3(-gr,1.0,0.0));
    ico.push_back( glm::vec3(-1.0,0.0,gr));

    ico.push_back( glm::vec3(0.0,gr,1.0));
    ico.push_back( glm::vec3(0.0,gr,-1.0));
    ico.push_back( glm::vec3(-gr,1.0,0.0));

    ico.push_back( glm::vec3(0.0,gr,-1.0));
    ico.push_back( glm::vec3(-1.0,0.0,-gr));
    ico.push_back( glm::vec3(-gr,1.0,0.0));

    ico.push_back( glm::vec3(-1.0,0.0,-gr));
    ico.push_back( glm::vec3(0.0,gr,-1.0));
    ico.push_back( glm::vec3(1.0,0.0,-gr));

    ico.push_back( glm::vec3(-1.0,0.0,-gr));
    ico.push_back( glm::vec3(1.0,0.0,-gr));
    ico.push_back( glm::vec3(0.0,-gr,-1.0));

    ico.push_back( glm::vec3(0.0,-gr,-1.0));
    ico.push_back( glm::vec3(1.0,0.0,-gr));
    ico.push_back( glm::vec3(gr,-1.0,0.0));

    ico.push_back( glm::vec3(0.0,-gr,-1.0));
    ico.push_back( glm::vec3(gr,-1.0,0.0));
    ico.push_back( glm::vec3(0.0,-gr,1.0));

    ico.push_back( glm::vec3(0.0,-gr,1.0));
    ico.push_back( glm::vec3(gr,-1.0,0.0));
    ico.push_back( glm::vec3(1.0,0.0,gr));



    for(size_t i=0;i<ico.size();i++)
        ico[i] = glm::normalize(ico[i]);

    for(int i=0;i<resolution;i++) {
        std::vector<glm::vec3> ico_refined;
        refineSolidSphere(ico,ico_refined);
        ico = ico_refined;
    }

    std::vector<glm::vec3> vertexWithNormal;
    GLuint id;


    for(size_t i=0;i<ico.size();i++) {
        vertices.push_back({ico[i],ico[i],glm::vec2{0,0}});
    }
}



Sphere::Sphere(glm::vec3 pos, int res) : DrawableObj(pos)
{
    initSolidSphere(res);
    initBuffers();
}

void Sphere::render(Shader &shader) const {
    // send ctm to shader
    glm::mat4 model;
    render(shader,model);
}

void Sphere::render(Shader &shader, glm::mat4 model) const {

    model= glm::translate(model,position);

    shader.setMat4("model",model);
    glBindVertexArray(this->VAO);
    glDrawArrays(GL_TRIANGLES, 0, vertices.size());
    glBindVertexArray(0);
}


Cube::~Cube() {

}

Cube::Cube(glm::vec3 pos) : DrawableObj(pos) {

    initBuffers();


}

void Cube::initBuffers() {

//    Vertex verts[]= {
//            {-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f},
//            { 0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f},
//            {0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f},
//            {0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f},
//            {-0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f},
//            {-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f},
//
//            {-0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f},
//            {0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f},
//            {0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f},
//            {0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f},
//            {-0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f},
//            {-0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f},
//
//            {-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f},
//            {-0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f},
//            {-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f},
//            {-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f},
//            {-0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f},
//            {-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f},
//
//            {0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f},
//            {0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f},
//            {0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f},
//            {0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f},
//            {0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f},
//            {0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f},
//
//            {-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f},
//            {0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f},
//            {0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f},
//            {0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f},
//            {-0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f},
//            {-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f},
//
//            {-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f},
//            {0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f},
//            {0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f},
//            {0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f},
//            {-0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f},
//            {-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f}
//    };
//
    float vertices[] = {
            -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
            0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
            0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
            0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
            -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,

            -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
            0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
            -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,

            -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
            -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
            -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
            -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
            -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
            -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

            0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
            0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
            0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
            0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
            0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

            -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
            0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
            0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
            0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

            -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
            0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
    };
    // first, configure the cube's VAO (and VBO)

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindVertexArray(VAO);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // normal attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(0);

}

void Cube::render(Shader &shader) const {

    // world transformation
    glm::mat4 model;
    render(shader,model);

}

void Cube::render(Shader &shader, glm::mat4 model) const {

    shader.use();
    model = glm::translate(model,position);
    shader.setMat4("model", model);
//    std::cout << "draw cube"<<std::endl;
    shader.setVec4("color",color);

    // render the cube
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);

}


Plane::Plane(glm::vec3 pos, float extend):DrawableObj(pos) {

    vertices.push_back({glm::vec3(0,0,0),glm::vec3(0,0,1),glm::vec2(0,0)});
    vertices.push_back({glm::vec3(extend,0,0),glm::vec3(0,0,1),glm::vec2(0,0)});
    vertices.push_back({glm::vec3(extend,extend,0),glm::vec3(0,0,1),glm::vec2(0,0)});

    vertices.push_back({glm::vec3(0,extend,0),glm::vec3(0,0,1),glm::vec2(0,0)});
    vertices.push_back({glm::vec3(0,0,0),glm::vec3(0,0,1),glm::vec2(0,0)});
    vertices.push_back({glm::vec3(extend,extend,0),glm::vec3(0,0,1),glm::vec2(0,0)});

    initBuffers();
}

void Plane::render(Shader &shader) const {
    render(shader,glm::mat4());
}

void Plane::render(Shader &shader, glm::mat4 model) const {
    shader.use();
    glm::mat4 trans;
    trans = glm::translate(trans,position);
    model = trans*model;
    shader.setMat4("model", model);

    // render the cube
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0,vertices.size());
    glBindVertexArray(0);
}

Mesh::Mesh(std::vector<Vertex> &vertices,  std::vector<GLuint> &indices):DrawableObj({0,0,0}){

    this->vertices=vertices;
    initBuffers();

}

Mesh::~Mesh() {
    glDeleteVertexArrays(1,&VAO);
    glDeleteBuffers(1,&VBO);
}



void Mesh::render(Shader &shader) const {
    glm::mat4 model;
    render(shader,model);
}

void Mesh::render(Shader &shader, glm::mat4 model) const {
    shader.use();
    shader.setVec4("color",color);
    shader.setMat4("model", model);

    glBindVertexArray(this->VAO);
//    glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
    glDrawArrays(GL_TRIANGLES, 0, vertices.size());
    glBindVertexArray(0);
}
