//
// Created by knork on 6/29/17.
//

#ifndef FLUIDSIM_SCENEDEFINITIONS_H
#define FLUIDSIM_SCENEDEFINITIONS_H
#include "../drawing/Scene.h"
#include "../drawing/DrawableGeometry.h"
#include "../physic/PhysicGeometry.h"

typedef std::unique_ptr<PhysicsObject> PhyPtr;
typedef std::unique_ptr<DrawableObj> DrawPtr;
typedef std::unique_ptr<LightCollection> LightPtr;

class SceneDefinitions {

public:
    static void first(Scene & scene);
    static void second(Scene & scene);

};


#endif //FLUIDSIM_SCENEDEFINITIONS_H
