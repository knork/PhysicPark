//
// Created by knork on 6/29/17.
//
#include "SceneDefinitions.h"
#include "../drawing/Scene.h"
#include "../drawing/DrawableGeometry.h"
#include "../physic/Fluid.h"



void SceneDefinitions::second(Scene &scene) {

    Shader &shader = scene.makeShader("regualV.glsl", "regularF.glsl");
    Shader &lightShader = scene.makeShader("lightV.glsl", "lightF.glsl");

//    new Simple(scene, glm::vec3(0, 0, 0), 5, 200000);
//    new Simple(scene, glm::vec3(30, 0, 30), 3, 5000);
//    new Simple(scene, glm::vec3(30, 0, 30),8, 2000);
//    new Simple(scene, glm::vec3(3, 10, 0), 3, 15);




    DrawPtr cube(new Cube(glm::vec3(30.0f, 40, 0.0f)));
    cube->color=glm::vec4(1.0,1.0,1.0,1.0);
//    DrawPtr cube2(new Cube(glm::vec3(150.0f, 50, 0.0f)));


    new Box(scene,glm::vec3(0,0,0),glm::vec3(17,500,75));

    Fluid *p_water = new Fluid(scene, glm::vec3(0.0f, 15, -0.0f), glm::vec3(0, 0, 0), 4700);


    scene.setKeyBoardCallback([&scene, p_water]() {
        GLFWwindow *win = scene.getWindow().getWindowPointer();
        if (p_water == nullptr) {
            std::cerr << "problem" << std::endl;
            return;
        }

        const float amount = 0.5f;

        if (glfwGetKey(win, GLFW_KEY_K) == GLFW_PRESS) {
            p_water->setK(p_water->getK() - 5 * amount * scene.getWindow().getDtUnpausable());
            std::cout << "Fluid K: " << p_water->getK() << std::endl;
        }

        if (glfwGetKey(win, GLFW_KEY_I) == GLFW_PRESS) {
            p_water->setK(p_water->getK() + 5 * amount * scene.getWindow().getDtUnpausable());
            std::cout << "Fluid K: " << p_water->getK() << std::endl;
        }

        if (glfwGetKey(win, GLFW_KEY_L) == GLFW_PRESS) {
            p_water->setK_near(p_water->getK_near() - amount * scene.getWindow().getDtUnpausable());
            std::cout << "Fluid K_near: " << p_water->getK_near() << std::endl;
        }
        if (glfwGetKey(win, GLFW_KEY_O) == GLFW_PRESS) {
            p_water->setK_near(p_water->getK_near() + amount * scene.getWindow().getDtUnpausable());
            std::cout << "Fluid K_near: " << p_water->getK_near() << std::endl;
        }

        if (glfwGetKey(win, GLFW_KEY_J) == GLFW_PRESS) {
            p_water->setRho_0(p_water->getRho_0() - 10 * amount * scene.getWindow().getDtUnpausable());
            std::cout << "Fluid Rho_0: " << p_water->getRho_0() << std::endl;
        }

        if (glfwGetKey(win, GLFW_KEY_U) == GLFW_PRESS) {
            p_water->setRho_0(p_water->getRho_0() + 10 * amount * scene.getWindow().getDtUnpausable());
            std::cout << "Fluid Rho_0: " << p_water->getRho_0() << std::endl;
        }

        if (glfwGetKey(win, GLFW_KEY_H) == GLFW_PRESS) {
            p_water->setH(p_water->getH() - amount * scene.getWindow().getDtUnpausable());
            std::cout << "Fluid H: " << p_water->getH() << std::endl;
        }

        if (glfwGetKey(win, GLFW_KEY_Y) == GLFW_PRESS) {
            p_water->setH(p_water->getH() + amount * scene.getWindow().getDtUnpausable());
            std::cout << "Fluid H: " << p_water->getH() << std::endl;
        }

        if (glfwGetKey(win, GLFW_KEY_R) == GLFW_PRESS) {
            p_water->reset();
        }

        if (glfwGetKey(win, GLFW_KEY_F) == GLFW_PRESS) {
            p_water->toggleParticleDisplay();
        }
        if (glfwGetKey(win, GLFW_KEY_G) == GLFW_PRESS) {
            p_water->toggleMarchingCube();
        }




    });


//
//    DrawPtr cube2( new Cube(glm::vec3(0.0f,5,0.0f),glm::vec3(0,0,0)));

    LightPtr lc(new LightCollection());
//    lc->attachLight(&(spheres[200]).get()->getParticles().at(0));
    lc->attachLight(&(*cube));
//    lc->attachLight(&(*cube2));

    scene.set(std::move(lc));
    scene.add(lightShader, std::move(cube));
//    scene.add(shader, std::move(cube2));

}