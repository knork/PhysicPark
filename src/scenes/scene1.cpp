//
// Created by knork on 6/29/17.
//
#include "SceneDefinitions.h"
#include "../drawing/Scene.h"
#include "../drawing/DrawableGeometry.h"
#include "../physic/Fluid.h"


void SceneDefinitions::first(Scene &scene) {

//    const int amount = 0;
//
//    Shader &shader = scene.makeShader("regualV.glsl", "regularF.glsl");
//
////    DrawPtr sphere(new Sphere(glm::vec3(-3.0f, 0.0f, -2.0f), 0));
//    PhyPtr sphere(new Simple(<#initializer#>, glm::vec3(0, 10, 0)));
//    PhyPtr spheres[amount];
//
//    for (int i = 0; i < amount; i++) {
//        float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
//        float y = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
//        r = r * 16 - 8;
//        y *= y * 8 - 4;
//        PhyPtr some(new Simple(<#initializer#>, glm::vec3(r, 16 + i % ((amount / 25) + 1), y)));
//        spheres[i] = std::move(some);
//    }
//
//    DrawPtr cube(new Cube(glm::vec3(-70.0f, 40, -130.0f)));
//    DrawPtr cube2(new Cube(glm::vec3(150.0f, 50, 0.0f)));
//    PhyPtr wall(new Wall(<#initializer#>, 2, glm::vec3(0, 0, 0)));
//    float extend = 5;
//    PhyPtr wall1(new Wall(<#initializer#>, 1, glm::vec3(-extend, 0, 0)));
//    PhyPtr wall2(new Wall(<#initializer#>, -1, glm::vec3(50, 0, 0)));
//    PhyPtr wall3(new Wall(<#initializer#>, -3, glm::vec3(0, 0, extend)));
//    PhyPtr wall4(new Wall(<#initializer#>, 3, glm::vec3(0, 0, -extend)));
//    Fluid *p_water = new Fluid(<#initializer#>, glm::vec3(-100, 0, -100), 0, glm::vec3(10, 10, 0));
//    PhyPtr water(p_water);
//
//
//    scene.setKeyBoardCallback([&scene,p_water]() {
//        GLFWwindow *win = scene.getWindow().getWindowPointer();
//        if(p_water== nullptr){
//            std::cerr<<"problem"<<std::endl;
//            return;
//        }
//
//        const float amount=0.5f;
//
//        if (glfwGetKey(win, GLFW_KEY_K) == GLFW_PRESS) {
//            p_water->setK(p_water->getK() -5*amount * scene.getWindow().getDtUnpausable());
//            std::cout << "Fluid K: "<<p_water->getK()<<std::endl;
//        }
//
//        if (glfwGetKey(win, GLFW_KEY_I) == GLFW_PRESS) {
//            p_water->setK(p_water->getK() + 5*amount * scene.getWindow().getDtUnpausable());
//            std::cout << "Fluid K: "<<p_water->getK()<<std::endl;
//        }
//
//        if (glfwGetKey(win, GLFW_KEY_L) == GLFW_PRESS) {
//            p_water->setK_near(p_water->getK_near() - amount * scene.getWindow().getDtUnpausable());
//            std::cout << "Fluid K_near: "<<p_water->getK_near()<<std::endl;
//        }
//        if (glfwGetKey(win, GLFW_KEY_O) == GLFW_PRESS) {
//            p_water->setK_near(p_water->getK_near() + amount * scene.getWindow().getDtUnpausable());
//            std::cout << "Fluid K_near: "<<p_water->getK_near()<<std::endl;
//        }
//
//        if (glfwGetKey(win, GLFW_KEY_J) == GLFW_PRESS) {
//            p_water->setRho_0(p_water->getRho_0() - 10*amount * scene.getWindow().getDtUnpausable());
//            std::cout << "Fluid Rho_0: "<<p_water->getRho_0()<<std::endl;
//        }
//
//        if (glfwGetKey(win, GLFW_KEY_U) == GLFW_PRESS) {
//            p_water->setRho_0(p_water->getRho_0() + 10*amount * scene.getWindow().getDtUnpausable());
//            std::cout << "Fluid Rho_0: "<<p_water->getRho_0()<<std::endl;
//        }
//
//        if (glfwGetKey(win, GLFW_KEY_H) == GLFW_PRESS) {
//            p_water->setH(p_water->getH() - amount * scene.getWindow().getDtUnpausable());
//            std::cout << "Fluid H: "<<p_water->getH()<<std::endl;
//        }
//
//        if (glfwGetKey(win, GLFW_KEY_Y) == GLFW_PRESS) {
//            p_water->setH(p_water->getH() + amount * scene.getWindow().getDtUnpausable());
//            std::cout << "Fluid H: "<<p_water->getH()<<std::endl;
//        }
//
//        if (glfwGetKey(win, GLFW_KEY_R) == GLFW_PRESS) {
//            p_water->reset();
//        }
//
//
//
//
//    });
//
//
//    scene.add(std::move(water));
//
////
////    DrawPtr cube2( new Cube(glm::vec3(0.0f,5,0.0f),glm::vec3(0,0,0)));
//
//    LightPtr lc(new LightCollection());
////    lc->attachLight(&(spheres[200]).get()->getParticles().at(0));
//    lc->attachLight(&(*cube));
//    lc->attachLight(&(*cube2));
//
//    scene.add(std::move(sphere));
//    for (int i = 0; i < amount; i++) {
//        scene.add(std::move(spheres[i]));
//    }
//    scene.add(shader, std::move(cube));
//    scene.add(shader, std::move(cube2));
//    scene.add(std::move(wall));
//    scene.add(std::move(wall1));
//    scene.add(std::move(wall2));
//    scene.add(std::move(wall3));
//    scene.add(std::move(wall4));
//
//    scene.set(std::move(lc));


}